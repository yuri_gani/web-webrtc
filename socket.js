const socketIO = require('socket.io');
const uuidV4 = require('uuid/v4');
const fs = require('fs');
var crypto = require('crypto');
var mime = require('mime');
const fileType = require('file-type');
const User = require('./model/User');
const Subscription = require('./model/Subscription');
const Roster = require('./model/Roster');
const config = require('./config');

const profilePictureDir = 'public/images/profilepicture/';

var instance = null;

function socketMain(serverHttps) {
    var io = socketIO(serverHttps);
    const ioMain = io;

    var connections = {};
    var calls = {};

    var rooms = {

    }

    function connectionsToArray(user) {
        if (typeof user === 'string') {
            // User.findOne({
            //     username: user
            // }).populate('roster').then((user) => {
            //     if (user) {
            //         console.log(user.roster.users);
            //     }
            // })
        } else if (typeof user === 'object') {
            if (user._id) {

            } else {

            }
        } else {
            var retval = Object.keys(connections);
            return retval;
        }
    }

    function deleteSocketId(socketId) {
        var isMain = false;
        for (var username in connections) {
            if (connections.hasOwnProperty(username)) {
                for (var index = connections[username].sockets.length - 1; index >= 0; index--) {
                    if (connections[username].sockets[index] == socketId) {
                        connections[username].sockets.splice(index, 1);
                        if (connections[username].sockets.length == 0) {
                            delete connections[username];

                            // send presence offline to subscribers
                            emitPresence(username, 'offline');
                            break;
                        } else {
                            if (connections[username].main == socketId) {
                                connections[username].main = connections[username].sockets[connections[username].sockets.length - 1];
                            }
                        }

                    }
                }
            }
        }
        return isMain;
    }

    function releaseCallFromSocketId(socketId){
        console.log(calls);
        for (var callid in calls) {
            if (calls.hasOwnProperty(callid)) {
                var call = calls[callid];
                if (call.from_id == socketId){
                    var target = call.to_id || 'user#'+call.to;
                    call = Object.assign(call, {
                        action: 'reject'
                    });
                    console.log('reject call', call, target);
                    // processCall(call);
                    io.to(target).emit('call', call);
                    delete calls[callid];
                }else if (call.to_id == socketId){
                    var target = call.from_id || 'user#'+call.from;
                    call = Object.assign(call, {
                        action: 'reject'
                    });
                    console.log('reject call', call, target);
                    // processCall(call);
                    io.to(target).emit('call', call);
                    delete calls[callid];
                }
            }
        }
    }

    function emitPresence(username, status, profile) {
        ioMain.to("subs#" + username).emit('roster', {
            type: 'presence',
            from: username,
            status: status,
            profile
        });
    }

    function register(socket, user) {
        if (!connections.hasOwnProperty(user.username)) {
            connections[user.username] = {
                presence: 'online',
                sockets: []
            };
        }
        console.log('register', user.username);
        socket.join('user#' + user.username);

        if (user.type == 'guest'){
            socket.join('subs#'+config.cs || 'dian');
        }
        connections[user.username].sockets.push(socket.id);
        connections[user.username].profile = {
            username: user.username,
            displayName: user.displayName,
            profilePicture: user.profilePicture
        };
        socket.emit("register", {
            status: true,
            profile: connections[user.username].profile
        });
        User.find({
            '_id': {
                $in: user.roster.users
            }
        }).lean().then((subTos) => {
            for (var index = 0; index < subTos.length; index++) {
                var subTo = subTos[index];
                socket.join('subs#' + subTo.username);
            }
        }).catch((err) => {
            console.error(err);
        });
    }

    function setMain(socket, username) {
        if (connections[username]) {
            connections[username].main = socket.id
        }
    }



    // const ioMain = io.of('/main');
    // const ioCall = io.of('/call');
    ioMain.on("connection", (socket) => {
        console.log(socket.id, "from", socket.conn.remoteAddress, "connected");

        socket.on("disconnect", () => {
            console.log(socket.id, "disconnected");
            releaseCallFromSocketId(socket.id);
            deleteSocketId(socket.id);

        });
        socket.on("error", () => {
            console.log("error", error);
            deleteSocketId(socket.id);
        });
        socket.on("register", (arg) => {
            arg = Object.assign({
                username: null,
                type: 'main'
            }, arg);

            if (arg.password) { // do login
                User.findOne({
                    username: arg.username
                }).select('+password').populate('roster').then(function(user) {
                    if (user) {
                        user.comparePassword(arg.password, function(err, isMatch) {
                            if (err)
                                console.error(err);
                            if (isMatch) {
                                // add to connections
                                register(socket, user);
                                emitPresence(user.username, 'online', connections[user.username].profile);
                            } else {
                                socket.emit("register", {
                                    status: false
                                });
                                socket.disconnect();
                            }
                        })
                    } else {
                        socket.emit("register", {
                            status: false
                        });
                        socket.disconnect();
                    }
                }).catch(function(err) {
                    console.error(err);
                    socket.emit("register", {
                        status: false
                    });
                    socket.disconnect();
                })
            } else if (arg.session_id) { // check session id
                sessionStore.get(arg.session_id, function(err, data) {
                    if (err) {
                        console.error(err);
                    } else if (data && arg.username == data.username && data.authenticated) {
                        User.findOne({
                            username: arg.username
                        }).populate('roster').then((user) => {
                            if (user) {
                                register(socket, user);
                                emitPresence(user.username, 'online', connections[user.username].profile);
                            } else {
                                socket.emit("register", {
                                    status: false
                                });
                                socket.disconnect();
                            }
                        })
                    } else {
                        socket.emit("register", {
                            status: false
                        });
                        socket.disconnect();
                    }

                });
            } else {
                socket.emit("register", {
                    status: false
                });
                socket.disconnect();
            }
        });

        socket.on("roster", (arg) => {
            var username = getSocketUsername(socket);
            if (username) {
                User.findOne({
                    username: username
                }).populate({
                    path: 'roster',
                    populate: {
                        path: 'users',
                    }
                }).lean().then((user) => {
                    // console.log('roster', username, user.roster.users);
                    var users = [];
                    for (var i = 0; i < user.roster.users.length; i++) {
                        var u = user.roster.users[i];
                        users.push({
                            _id: u._id,
                            username: u.username,
                            displayName: u.displayName,
                            profilePicture: u.profilePicture,
                            online: !!connections[u.username]
                        });
                    }
                    console.log(users);
                    setTimeout(function() {
                        socket.emit("roster", users);
                    }, 1000);
                }).catch((err) => {
                    console.error(err);
                });
            }
        })


        socket.on('message', (arg) => {
            console.log('message', arg);
            arg = Object.assign({
                id: uuidV4(),
                type: null,
                to: null,
                action: null
            }, arg);

            switch (arg.type) {
                case 'call':
                    processCall(arg, socket);
                    break;
                default:
                    break;
            }

        });

        socket.on('call', (arg) => {

            var username = getSocketUsername(socket);
            if (username) {
                arg = Object.assign({
                    type: 'call', // 'call', 'sdp', 'ice'
                    to: null,
                    action: null,
                }, arg);
                if (arg.type == 'call') {
                    arg.media = arg.media || 'video';
                }

                processCall(arg, socket);
            }
        })

        socket.on('subscription', (arg) => {
            var username = getSocketUsername(socket);
            if (username) {
                console.log('subscription', arg);
                User.findOne({
                        username
                    })
                    .populate({
                        path: 'roster',
                        populate: {
                            path: 'subscriptions'
                        }
                    })
                    .then((from) => {
                        if (from) {
                            if (arg.type == 'subscribe' || arg.type == 'unsubscribe') {
                                User.findOne({
                                        username: arg.to
                                    })
                                    .populate("roster")
                                    .then((to) => {
                                        if (arg.type == 'subscribe') { // subscribe
                                            if (to._id.equals(from._id)) {
                                                socket.emit('subscription', {
                                                    from: from.username,
                                                    to: to.username,
                                                    type: 'unsubscribe',
                                                    status: 'rejected'
                                                });
                                                return;
                                            }
                                            // find if already subscribed
                                            // search in roster
                                            var foundInRoster = false;
                                            for (var i = 0; i < from.roster.users.length; i++) {
                                                var rUser = from.roster.users[i];
                                                if (rUser.equals(to._id)) {
                                                    foundInRoster = true;
                                                    break;
                                                }
                                            }
                                            if (foundInRoster) { // already accepted
                                                console.log(from.username, "found", to.username, "in roster");
                                                socket.emit('subscription', {
                                                    type: 'subscribe',
                                                    from: from.username,
                                                    to: to.username,
                                                    status: 'accepted'
                                                })
                                                return;
                                            }
                                            // search in subscription
                                            var foundInSubscription = false;
                                            for (var i = 0; i < from.roster.subscriptions.length; i++) {
                                                var rSub = from.roster.subscriptions[i];
                                                if (rSub.to.equals(to._id)) { // already exists
                                                    socket.emit('subscription', {
                                                        type: 'subscribe',
                                                        from: from.username,
                                                        to: to.username,
                                                        status: 'pending'
                                                    })
                                                    foundInSubscription = true;
                                                    break;
                                                } else if (rSub.from.equals(to._id)) { // accept subscription
                                                    rSub.status = 'accepted';
                                                    rSub.save().then(() => {
                                                        from.roster.users.push(to._id);
                                                        to.roster.users.push(from._id);
                                                        from.roster.save();
                                                        to.roster.save();
                                                        var sub = {
                                                            type: 'subscribe',
                                                            from: from.username,
                                                            to: to.username,
                                                            status: 'accepted'
                                                        };

                                                        // join each other subs room
                                                        connections[sub.from].sockets.forEach(function(element) {
                                                            ioMain.sockets.sockets[element].join('subs#' + sub.to);
                                                        }, this);
                                                        connections[sub.to].sockets.forEach(function(element) {
                                                            ioMain.sockets.sockets[element].join('subs#' + sub.from);
                                                        }, this);

                                                        socket.to('user#' + arg.to).emit('subscription', sub);
                                                        socket.emit('subscription', sub);
                                                    }).catch((err) => {
                                                        console.error(err);
                                                    });
                                                    foundInSubscription = true;
                                                    break;
                                                }
                                            }
                                            if (!foundInSubscription) {
                                                // create new
                                                var newSub = new Subscription({
                                                    from: from._id,
                                                    to: to._id
                                                });
                                                newSub.save().then((sub) => {
                                                    from.roster.subscriptions.push(sub._id);
                                                    to.roster.subscriptions.push(sub._id);
                                                    from.roster.save();
                                                    to.roster.save();
                                                    var sub = {
                                                        type: 'subscribe',
                                                        from: from.username,
                                                        to: to.username,
                                                        status: 'pending',
                                                        fromProfile: {
                                                            username: from.username,
                                                            displayName: from.displayName,
                                                            profilePicture: from.profilePicture,
                                                        },
                                                        toProfile: {
                                                            username: to.username,
                                                            displayName: to.displayName,
                                                            profilePicture: to.profilePicture,
                                                        }
                                                    };
                                                    socket.to('user#' + arg.to).emit('subscription', sub);
                                                    socket.emit('subscription', sub);
                                                });
                                            }


                                        } else { // unsubscribe
                                            // search in roster
                                            var foundInRoster = false;
                                            for (var i = 0; i < from.roster.users.length; i++) {
                                                var rUser = from.roster.users[i];
                                                if (rUser.equals(to._id)) {
                                                    from.roster.users.splice(i, 1);
                                                    foundInRoster = true;
                                                    break;
                                                }
                                            }
                                            from.roster.save().then(() => {
                                                console.log('delete user', to.username, 'from', from.username, `'s roster`);
                                            });

                                            if (foundInRoster) {
                                                for (var i = 0; i < to.roster.users.length; i++) {
                                                    var rUser = to.roster.users[i];
                                                    if (rUser.equals(from._id)) {
                                                        to.roster.users.splice(i, 1);
                                                        break;
                                                    }
                                                }
                                                to.roster.save().then(() => {
                                                    console.log('delete user', from.username, 'from', to.username, `'s roster`);
                                                });
                                                var sub = {
                                                    type: 'unsubscribe',
                                                    from: from.username,
                                                    to: to.username
                                                }
                                            }
                                            Subscription.remove({
                                                $or: [{
                                                        from: from._id,
                                                        to: to._id
                                                    },
                                                    {
                                                        to: from._id,
                                                        from: to._id
                                                    },
                                                ]
                                            }).then(() => {
                                                var sub = {
                                                    type: 'unsubscribe',
                                                    from: from.username,
                                                    to: to.username,
                                                    status: 'rejected',
                                                    fromProfile: {
                                                        username: from.username,
                                                        displayName: from.displayName,
                                                        profilePicture: from.profilePicture,
                                                    },
                                                    toProfile: {
                                                        username: to.username,
                                                        displayName: to.displayName,
                                                        profilePicture: to.profilePicture,
                                                    }
                                                }
                                                socket.to('user#' + arg.to).emit('subscription', sub);
                                                socket.emit('subscription', sub);
                                            }).catch((err) => {
                                                console.error(err);
                                            });

                                            // leave each other subs room
                                            connections[sub.from].sockets.forEach(function(element) {
                                                ioMain.sockets.sockets[element].leave('subs#' + sub.to);
                                            }, this);
                                            connections[sub.to].sockets.forEach(function(element) {
                                                ioMain.sockets.sockets[element].leave('subs#' + sub.from);
                                            }, this);

                                            // socket.to('user#' + arg.to).emit('subscription', sub);
                                            // socket.emit('subscription', sub);

                                        }

                                    }).catch((err) => {
                                        var username = getSocketUsername(socket);
                                        socket.emit('subscription', {
                                            from: username,
                                            to: arg.to,
                                            type: 'unsubscribe',
                                            status: 'rejected'
                                        });
                                    });
                            } else if (arg.type == 'list') {
                                Subscription.find({
                                        $and: [{
                                            status: 'pending'
                                        }, {
                                            $or: [{
                                                    from: from._id,
                                                    status: 'pending'
                                                },
                                                {
                                                    to: from._id,
                                                    status: 'pending'
                                                },
                                            ]
                                        }],
                                    })
                                    .populate('from', '-roster -created -status -_id -__v')
                                    .populate('to', '-roster -created -status -_id -__v')
                                    .lean()
                                    .then((subMs) => {
                                        var subs = [];
                                        for (var index = 0; index < subMs.length; index++) {
                                            var subM = subMs[index];
                                            var sub = {
                                                from: subM.from.username,
                                                to: subM.to.username,
                                                status: subM.status,
                                                fromProfile: subM.from,
                                                toProfile: subM.to
                                            };

                                            subs.push(sub);
                                        }
                                        socket.emit('subscription', {
                                            type: 'list',
                                            data: subs
                                        });
                                    }).catch((err) => {
                                        console.error(err);
                                    });
                            }

                        }
                    }).catch((err) => {
                        throw err;
                    })
            }

        });

        socket.on('profilePicture', (arg) => {
            var username = getSocketUsername(socket);
            if (username) {
                User.findOne({
                    username: getSocketUsername(socket)
                }).then((user) => {
                    if (Buffer.isBuffer(arg)) {
                        var ft = fileType(arg);
                        if (ft.mime.startsWith('image')) {
                            var tempName = profilePictureDir + 'temp-' + Date.now();
                            console.log('profilePicture', 'tempName', tempName);
                            fs.writeFile(tempName, arg, (err) => {
                                if (err) {
                                    console.error(err);
                                    return;
                                }
                                crypto.pseudoRandomBytes(16, function(err, raw) {
                                    if (err) {
                                        console.error(err);
                                        return;
                                    }
                                    var fn = profilePictureDir + Date.now() + '-' + raw.toString('hex') + '.' + ft.ext;

                                    console.log('profilePicture', 'newName', fn);
                                    fs.rename(tempName, fn, (err) => {
                                        if (err) {
                                            console.error(err);
                                            return;
                                        }

                                        var newProfilePicturePath = fn.replace("public/", "/");
                                        user.profilePicture = newProfilePicturePath;
                                        user.save().then((doc) => {
                                            console.log("update user", doc);
                                            socket.emit('profilePicture', {
                                                status: true,
                                                data: newProfilePicturePath
                                            });
                                        }).catch((err) => {
                                            console.error(err);
                                        });
                                    });

                                });
                            });
                        }
                    } else if (typeof arg === 'string') {
                        console.log("profilePicture", "unsupported");
                    } else {

                    }
                }).catch((err) => {
                    console.error(err);
                });
            }
        })

        socket.on("displayName", (arg) => {
            var username = getSocketUsername(socket);
            if (username) {
                User.update({
                    username: getSocketUsername(socket)
                }, {
                    displayName: arg
                }).then((doc) => {
                    socket.emit('displayName', arg);
                }).catch((err) => {
                    console.error(err);
                });
            }
        });

        socket.on('query', (arg) => {
            var username = getSocketUsername(socket);
            if (username) {
                var res = arg;
                console.log("incoming query request", arg);
                switch (arg.type) {
                    case 'profileFromUsername':
                        profileFromUsername(arg.data).then((profile) => {
                            res.result = profile;
                            console.log("sending query result", res);
                            socket.emit('query', res);
                        }).catch((err) => {
                            res.result = false;
                            res.error = err;
                            console.log("sending query result", res);
                            socket.emit('query', res);
                        });
                        break;
                }
            }

        });
    })

    function processCall(call, socket) {
        console.log('processCall', call);
        console.log('from', connections[call.from]);
        console.log('to', connections[call.to]);

        let username = getSocketUsername(socket);
        call.from = call.from || username;

        if (call.from == username) { // from
            call.from_id = call.from_id || socket.id;
        } else {
            call.to_id = call.to_id || socket.id;
        }
        if (call.from == call.to) {

        } else if (connections[call.to] && connections[call.from]) {
            let target = call.to;
            if (username == call.from) {
                target = call.to;
            } else {
                target = call.from;
            }

            if (call.type == 'call') {
                calls[call.id] = call;
                if (call.action == 'calling') {

                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    setTimeout(function() {
                        console.log("check timout call", call, calls[call.id]);
                        if (typeof calls[call.id] === 'object' && calls[call.id].action == 'calling') {
                            // end call, it's timed out
                            call.action == 'reject';
                            call.reason == 'timeout';
                            socket.to('user#' + target).emit('call', call);
                            socket.emit('call', call);
                        }
                    }, 10000);
                    return;
                } else if (call.action == 'accept') {
                    if (username == call.from) return;
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                } else if (call.action == 'reject') {
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    delete calls[call.id];
                    return;
                }
            } else if (call.type == 'media') {
                if (call.action == 'ready') {
                    calls[call.id].ready = calls[call.id].ready || {};
                    calls[call.id].ready[getSocketUsername(socket)] = true;
                    if (calls[call.id].ready[call.from] && calls[call.id].ready[call.to]) {
                        socket.emit('call', call);
                        socket.to('user#' + target).emit('call', call);
                    }

                    // socket.to('user#' + target).emit('call', call);
                    return;
                } else if (call.action == 'sdp') {
                    console.log("send sdp to: ", target, " : ", call);
                    socket.to('user#' + target).emit('call', call);
                    return;
                } else if (call.action == 'ice') {
                    console.log("send ice to: ", target, " : ", call);
                    socket.to('user#' + target).emit('call', call);
                    return;
                }
            }
        } else {
            call.action = 'unavailable';
            socket.emit('call', call);
            return;
        }
        console.log('processCall', call, 'invalid');
        call.action = 'invalid';
        socket.emit('call', call);
    }

    function processChat(chat, socket) {
        var username = getSocketUsername(socket);
        var target = null;
        if (username == socket.from) { // regular
            target = chat.to;
            if (connections[target]) {
                socket.to("user#" + target).emit('chat', chat);
            } else {
                storeChat(chat, username, target);
            }
        } else { // status | ack
            target = username;
            if (connections[target]) {
                socket.to("user#" + target).emit('chat', chat);
            } else {
                storeChat(chat, username, target);
            }
        }
    }

    function storeChat(chat, username, target) {
        // TODO: implement store and send
    }

    function getSocketUsername(socket) {
        for (var room in socket.rooms) {
            if (socket.rooms.hasOwnProperty(room)) {
                if (room.startsWith('user#')) {
                    return room.substr(5);
                }
            }
        }
        return null;
    }

    function profileFromUsername(username) {
        return new Promise((resolve, reject) => {
            User.findOne({
                username
            }).lean().then((user) => {
                resolve({
                    username: user.username,
                    displayName: user.displayName,
                    profilePicture: user.profilePicture
                });
            }).catch((err) => {
                reject(err);
            });
        })
    }

    instance = io;
}

socketMain.getInstance = function(){
    return instance;
}



module.exports = socketMain;