var express = require('express');
var router = express.Router();
var config = require('../config');



var User = require('../model/User');

function getAvailableOperators(){
    return [];   
}

router.get('/', function(req, res, next){
    var email = req.query.email || "null";
    var name = req.query.name || "null";
    var phone = req.query.phone || "null";
    var media = req.query.media || "audio";
    console.log("email", email)
    console.log("name", name);
    console.log("phone", phone);
    console.log("media", phone);

    // if (req.session.authenticated){
    //     res.redirect('/call/'+ config.cs || 'dian');
    //     return;
    // }

    User.findOne({
        username: email,
        type: 'guest'
    }).then((user)=>{
        if (!user){
            console.log('/callcs', email, 'user not found');
            user = new User({
                username: email,
                password: email,
                displayName: name,
                type: 'guest'
            });
            user.save(function(err){
                console.error('/callcs', err);
            });    
        }else{
            console.log('/callcs', email, 'user found');
        }
        req.session.username = user.username;
        req.session.displayName = user.displayName;
        req.session.phone = phone;
        req.session.authenticated = true;
        req.session.guest = true;


        res.render('callcs',{
            title: 'Click2Call',
            username: req.session.username,
            displayName: req.session.displayName,
            sessionId: req.session.id,
            profilePicture: req.session.profilePicture ? req.session.profilePicture : '/images/profilepicture/default_profile_picture.png',
            target: config.cs || 'dian',
            media
        });
    }).catch((error)=>{
        console.error('/callcs', error);
        res.redirect('/');
    })
    
});


router.get('/operators', function(req, res, next){
    var io = require('../socket').getInstance();

    User.find({
        username: config.cs || 'dian'
    }).lean().then((operators)=>{
        console.log('operators', operators);
        var operatorsTosend = [];
        for (var index = 0; index < operators.length; index++) {
            var operator = operators[index];
            operatorsTosend.push({
                username: operator.username,
                displayName: operator.displayName,
                profilePicture: operator.profilePicture,
                online: typeof io.sockets.adapter.rooms['user#' +( operator.username || 'dian')] !== 'undefined'
            });
        }
        res.send({
            status: true,
            operators: operatorsTosend
        });
    }).catch((error)=>{
        console.error('operators', error);
        res.send({
            status: false,
            operators: []
        });
    })
    
})



module.exports = router;

