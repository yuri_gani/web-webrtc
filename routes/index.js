var express = require('express');
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var User = require('../model/User');
var router = express.Router();
var discStorage = multer.diskStorage({
    destination: 'public/images/profilepicture/',
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            var fn = Date.now() + '-' + raw.toString('hex') + '.' + mime.extension(file.mimetype);
            console.log('upload filename', fn, mime.extension(file.mimetype));
            cb(null, fn);
        });
    }
});
var uploadProfilePicture = multer({
    storage: discStorage,
    fileFilter: function(req, file, cb) {
        cb(null, file.mimetype.startsWith("image"));
    }
});


/* GET home page. */
router.get('/', function(req, res, next) {

    res.render('index', {
        title: 'Click2Call',
        username: req.session.username,
        displayName: req.session.displayName,
        sessionId: req.session.id,
        profilePicture: req.session.profilePicture ? req.session.profilePicture : '/images/profilepicture/default_profile_picture.png',
    });
});

router.get('/call/:remoteUsername', function(req, res, next) {
    res.render('index', {
        title: 'Click2Call',
        username: req.session.username,
        displayName: req.session.displayName,
        sessionId: req.session.id,
        profilePicture: req.session.profilePicture ? req.session.profilePicture : '/images/profilepicture/default_profile_picture.png',
        target: req.params.remoteUsername,
    });
});

router.post('/profilepicture/upload', function(req, res, next) {
    uploadProfilePicture.single('profile-picture')(req, res, function(err) {
        var newProfilePicturePath = req.file.path.replace("public/", "/");
        User.update({
            username: req.session.username
        }, {
            profilePicture: newProfilePicturePath
        }).then((doc) => {
            console.log("update user", doc);
            req.session.profilePicture = newProfilePicturePath;
            req.session.save(() => {
                res.redirect('/');
            })

        }).catch((err) => {
            console.error(err);
        });
    })
});


router.auth = function(req, res, next) {
    if (!req.session) {
        throw 'need session'
    }
    if (req.url.startsWith("/login")) {
        next();
        return;
    }

    if (!req.session.authenticated) {
        req.session.nextUrl = req.url;
        res.redirect('/login');
        return;
    }
    next();
    return;
}
module.exports = router;