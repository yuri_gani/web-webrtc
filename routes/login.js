var config = require('../config');

var express = require('express');
var router = express.Router();
var User = require('../model/User');




if (config.ldap && config.ldap.url){
    var LDAP = require('ldap-client');
    var ldapClient = new LDAP({
        uri: config.ldap.url,
        base: ''
    })
}

var loginLdap = function(username, password){
    return new Promise((resolve, reject) => {
        if (config.ldap && config.ldap.url){
            ldapClient.bind(
                {
                    binddn: username,
                    password: password
                }, function(err, data){
                if (err){
                    reject(new Error("Auth Error"));
                    return;
                }

                ldapClient.search({
                    filter: `(uid=${username})`,
                    attrs:'telkomnik cn mail telkomdivision department title'
                }, function(err, searchRes){
                        if (err){
                            console.error('ldap', err);
                            resolve({
                                telkomnik: username
                            })
                            return;
                        }
                        console.log('searchRes', searchRes);
                        if (Array.isArray(searchRes)){
                            searchRes = searchRes.length > 0 ? searchRes[0] : null;
                        }

                        if (searchRes){
                            resolve({
                                telkomnik: searchRes.telkomnik[0],
                                cn: searchRes.cn[0],
                                mail: searchRes.mail[0],
                                telkomdivision: searchRes.telkomdivision[0],
                                department: searchRes.department[0],
                                title: searchRes.title[0]
                            });
                        }else{
                            resolve({
                                telkomnik: username
                            });
                        }
                        
                });
            });
        }else{
            reject(new Error("LDAP not configured"));
        }
    });
}

// var ldapClient = ldap.createClient({
//     url: config.ldap.url
// });

function isLoggedIn(req, res, next) {
    if (!req.session) {
        throw 'need session';
    }
    if (req.session.authenticated) {
        if (req.session.nextUrl) {
            var nextUrl = req.session.nextUrl;
            delete req.session.nextUrl;
            res.redirect(nextUrl);
            return;
        } else {
            res.redirect('/');
            return;
        }
    }
    next();
}

/* GET home page. */
router.get('/', isLoggedIn, function(req, res, next) {
    res.render('login', { title: 'Click2Call : login' });
});
router.post('/', isLoggedIn, function(req, res, next) {
    console.log('login', req.body.username);
    User.findOne({
        username: req.body.username
    }).select('+password').then(function(fUser) {
        if (fUser) {
            console.log('user exists');
            if (fUser.type == 'ldap'){
                loginLdap(fUser.username, req.body.password).then((data)=>{
                    console.log('ldap auth ok', req.body.username);
                    req.session.username = req.body.username;
                    req.session.displayName = fUser.displayName;
                    req.session.profilePicture = null;
                    req.session.authenticated = true;
                    req.session.ldap = userInfo;  
                }).catch((error)=>{
                    res.redirect(req.url);
                })
            }else{
                fUser.comparePassword(req.body.password, (err, isMatch) => {
                    if (err) {
                        console.error(err);
                    }
                    
                    if (isMatch) {
                        req.session.username = req.body.username;
                        req.session.displayName = fUser.displayName;
                        req.session.profilePicture = fUser.profilePicture;
                        req.session.authenticated = true;
    
                        isLoggedIn(req, res, next);
                    } else {
                        res.redirect(req.url);
                    }
                });
            }
            
        } else if (config.ldap && config.ldap.url) { // try ldap user
            loginLdap(req.body.username, req.body.password).then((userInfo)=>{
                console.log('ldap auth ok', req.body.username);
                req.session.username = req.body.username;
                req.session.displayName = userInfo.cn;
                req.session.profilePicture = null;
                req.session.authenticated = true;
                req.session.ldap = userInfo;

                fUser = new User({
                    username: req.body.username,
                    password: req.body.password,
                    displayName: userInfo.cn || req.body.username,
                    type: 'ldap' 
                });
                fUser.save(function(err){
                    isLoggedIn(req, res, next);
                    if (err){
                        console.error('failed to insert ldap user', err);
                    }
                })
            }).catch((error)=>{
                console.error('ldap login', error);
                res.redirect(req.url);
            })
            // ldapClient.bind(
            //     {
            //         binddn: req.body.username,
            //         password: req.body.password
            //     }, function(err, data){
            //     if (err){
            //         console.error('ldap error', err);
            //         res.redirect(req.url);
            //         return;
            //     }

            //     req.session.username = req.body.username;
            //     req.session.profilePicture = null;
            //     req.session.authenticated = true;

            //     ldapClient.search({
            //         filter: `(uid=${req.session.username})`,
            //         attrs:'telkomnik cn mail telkomdivision department title'
            //     }, function(err, searchRes){
            //             if (err){
            //                 console.error('ldap', err);
            //                 return;
            //             }
            //             console.log('searchRes', searchRes);
            //             req.session.ldap = {
            //                 telkomnik: searchRes.telkomnik[0],
            //                 cn: searchRes.cn[0],
            //                 mail: searchRes.mail[0],
            //                 telkomdivision: searchRes.telkomdivision[0],
            //                 department: searchRes.department[0],
            //                 title: searchRes.title[0]
            //             }

            //             var user = new User({
            //                 username: req.session.username,
            //                 displayName: req.session.ldap.cn,
            //                 password:'---'
            //             });
            //             user.save();
            //     });

                // isLoggedIn(req, res, next);                
            // });
        }else{
            console.log('user not exists');
            res.redirect(req.url);
        } 
    }).catch(function(err) {
        console.error(err);
        res.redirect(req.url);
    });
});

module.exports = router;