var express = require('express');
var router = express.Router();
var User = require('../model/User');
var Subscription = require('../model/Subscription');

var adminPassses = {
    "yuri": "hantubelaumakantahu",
    "andri": "gunduruwobapaketuwo",
}

function notLoggedInMid(req, res, next) {
    if (!req.session) {
        throw 'need session';
    }

    if (req.session.admin) {
        if (req.session.nextUrl) {
            var nextUrl = req.session.nextUrl;
            delete req.session.nextUrl;
            res.redirect(nextUrl);
            return;
        } else {
            res.redirect('/admin');
            return;
        }
    }
    next();
}

function loggedInMid(req, res, next) {
    if (!req.session) {
        throw 'need session';
    }
    console.log("loggedInMid");
    if (req.session.admin) {
        delete req.session.nextUrl;
        next();
        return;
    }
    res.redirect('/admin/login');

}

/* GET home page. */
router.get('/login', notLoggedInMid, function(req, res, next) {
    res.render('admin-login', { title: 'Admin' });
});
router.post('/login', notLoggedInMid, function(req, res, next) {
    console.log(req.body);
    if (adminPassses[req.body.username] === req.body.password) {
        req.session.admin = {
            username: req.body.username
        };

        notLoggedInMid(req, res, next);
        return;
    }
    res.redirect("/admin/login");
});

router.get('/logout', function(req, res, next) {
    req.session.admin = false;
    res.redirect('/admin');
})

router.get('/', loggedInMid, function(req, res, next) {
    res.render('admin', { title: 'indihomeApp: Admin' });
});

router.get('/users', loggedInMid, function(req, res, next) {
    User.find().lean().then(function(usrs) {
        res.json({
            status: true,
            users: usrs
        });
    }).catch(function(err) {
        res.json({
            status: false
        });
    });
});

router.post('/users', loggedInMid, function(req, res, next) {
    var param = req.body;
    console.log(param);
    var user = new User(param);
    user.save().then(function(data) {
        res.json({
            status: true,
            user: data
        });
    }).catch(function(err) {
        res.json({
            status: false,
            error: err
        });
    })
});
router.delete('/users/:id', loggedInMid, function(req, res) {
    var param = req.params;
    console.log(param);
    User.findOne({
        _id: param.id
    }).remove().then(function(data) {
        res.json({
            status: true
        });
    }).catch(function(err) {
        console.error(err);
        res.json({
            status: false,
            error: err
        });
    })
});


module.exports = router;