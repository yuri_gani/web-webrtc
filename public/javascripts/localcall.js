var pc1, pc2;
var stream1, stream2;
var sdp1, sdp2;
var video1, video2;




$(() => {
    pc1 = new RTCPeerConnection();
    pc1.onaddstream = pc1RemoteStreamAdded;
    pc1.onicecandidate = pc1onicecandidate;
    pc1.onremovestream = pc1onremovestream;
    pc1.oniceconnectionstatechange = pc1oniceconnectionstatechange;

    pc2 = new RTCPeerConnection();
    pc2.onaddstream = pc2RemoteStreamAdded;
    pc2.onicecandidate = pc2onicecandidate;
    pc2.onremovestream = pc2onremovestream;

    pc2.oniceconnectionstatechange = pc2oniceconnectionstatechange


    video1 = $('#localVideo').get(0);
    video2 = $('#remoteVideo').get(0);

    navigator.getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    if (navigator.getUserMedia) {
        navigator.getUserMedia({
                video: true,
                audio: false
            },
            function(stream) {
                video1.src = window.URL.createObjectURL(stream);
                video1.onloadedmetadata = function(e) {
                    console.log("playing");
                };
                stream1 = stream;

                pc1.addStream(stream1);

                pc1.createOffer((sdp) => {
                    sdp1 = sdp;
                    pc1.setLocalDescription(sdp1);
                    pc2.setRemoteDescription(new RTCSessionDescription(sdp1));
                    pc2.createAnswer().then(
                        onCreateAnswerSuccess,
                        onCreateSessionDescriptionError
                    );

                }, (err) => {
                    console.log(err);
                })
            },
            function(error) {
                console.err('startMedia', error)
                debugSection.append("startMedia error\n");
            }
        );
    } else {
        alert('Sorry, the browser you are using doesn\'t support getUserMedia');
        return;
    }

})

function pc1RemoteStreamAdded(event) {}

function pc1onicecandidate(event) {
    if (event.candidate) {
        pc2.addIceCandidate(event.candidate).then(() => {
            console.log("pc2 success add ice candidate");
        }, (e) => {
            console.log("pc2 failed add ice candidate", e);
        });
    }
}

function pc1onremovestream(event) {}

function pc1oniceconnectionstatechange(event) {
    console.log("pc1 on ice connection change", event);
}

function pc2RemoteStreamAdded(event) {
    stream2 = event.stream;
    video2.src = window.URL.createObjectURL(stream2);
    console.log("pc2 remoteStreamAdded");
}

function pc2onicecandidate(event) {
    if (event.candidate) {
        pc1.addIceCandidate(event.candidate).then(() => {
            console.log("pc1 success add ice candidate");
        }, (e) => {
            console.log("pc1 failed add ice candidate", e);
        });
    }
}

function pc2onremovestream(event) {}

function pc2oniceconnectionstatechange(event) {
    console.log("pc2 on ice connection change", event);
}

function onCreateAnswerSuccess(sdp) {
    pc2.setLocalDescription(sdp).then(() => {
        console.log("pc2 local description");
    }, (err) => {
        console.log("pc2 set Local description error", err);
    });
    pc1.setRemoteDescription(sdp).then(() => {
        console.log("pc1 remote description");
    }, (err) => {
        console.log("pc1 set remote description error", err);
    });
}

function onCreateSessionDescriptionError(err) {
    console.log("pc2 cretate session description error", err);
}