var socket;
var localVideo, remoteVideo;
var localStream, remoteStream;
var peerConnection;
var role;
var pcConfig = {
    'iceServers': [{
        'url': 'stun:stun.l.google.com:19302'
    }, {
        url: 'turn:numb.viagenie.ca',
        credential: 'physic',
        username: 'hyuricane@gmail.com'
    }, ]
};

$(() => {
    localVideo = $("#localVideo").get(0);
    remoteVideo = $("#remoteVideo").get(0);

    socket = io('/call');
    socket.on('connect', () => {
        console.log('connect');
        socket.emit('register', {
            username,
            room
        });
    });

    socket.on('reconnect', () => {
        console.log('reconnect');
    });

    socket.on('start', (startObj) => {

        role = startObj.role;
        console.log('role', role);
        createPeerConnection();
        if (role == 'caller') {
            doOffering();
        } else if (role == 'callee') {

        } else {

        }
    })

    socket.on('message', (message) => {
        if (message.from == username) {
            return;
        }
        console.log('receive message', message);
        switch (message.type) {
            case 'iceCandidate':
                receiveIceCandidate(message.data);
                break;
            case 'sdp':
                receiveSdp(message.data);
                break;
            default:
                break;
        }
    })
})

function createPeerConnection() {
    console.log('creating peer Connection');
    peerConnection = new RTCPeerConnection(pcConfig);
    peerConnection.onaddstream = onaddstream;
    peerConnection.onicecandidate = onicecandidate;
    peerConnection.oniceconnectionstatechange = oniceconnectionstatechange;
    peerConnection.onicegatheringstatechange = onicegatheringstatechange;
    peerConnection.onnegotiationneeded = onnegotiationneeded;
    peerConnection.onremovestream = onremovestream;
    peerConnection.onsignalingstatechange = onsignalingstatechange;
}

function startMedia(fn) {
    navigator.getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    if (navigator.getUserMedia) {
        navigator.getUserMedia({
                video: true,
                audio: true
            },
            function(stream) {
                localStream = stream;
                localVideo.src = window.URL.createObjectURL(localStream);
                fn(localStream);
            },
            function(error) {
                console.log(JSON.stringify(error));
                console.log(error.message);

                // console.log('startMedia ' + error)
                // console.log(JSON.stringify(error));
            }
        );
    } else {
        alert('Sorry, the browser you are using doesn\'t support getUserMedia');
        return;
    }
}

function doOffering() {
    startMedia((localStream) => {
        peerConnection.addStream(localStream);
        peerConnection.createOffer().then((localSdp) => {
            peerConnection.setLocalDescription(localSdp);

            offerSdp(localSdp);
        }, (err) => {
            console.error("failed do offering", err)
        })
    })

}

function receiveSdp(remoteSdp) {
    if (role == 'caller') {
        peerConnection.setRemoteDescription(remoteSdp);
    } else {
        doAnswering(remoteSdp);
    }
}

function doAnswering(sdp) {
    peerConnection.setRemoteDescription(sdp);
    startMedia((localStream) => {
        peerConnection.addStream(localStream);
        peerConnection.createAnswer().then((localSdp) => {
            peerConnection.setLocalDescription(localSdp);
            offerSdp(localSdp);
        }, (err) => {
            console.error("failed do answering", err)
        })
    })

}

function onaddstream(event) {
    remoteStream = event.stream;
    remoteVideo.src = window.URL.createObjectURL(remoteStream);
}

function onicecandidate(event) {
    if (event.candidate) {
        sendIceCandidate(event.candidate);
    } else {
        console.log('gathering ice candidates finished')
    }
}

function oniceconnectionstatechange(event) {}

function onicegatheringstatechange(event) {}

function onnegotiationneeded(event) {}

function onremovestream(event) {}

function onsignalingstatechange(event) {}

function sendIceCandidate(candidate) {
    console.log('sending ice candidate', {
        from: username,
        room: room,
        type: 'iceCandidate',
        data: candidate
    });
    socket.send({
        from: username,
        room: room,
        type: 'iceCandidate',
        data: candidate
    })
}

function receiveIceCandidate(candidate) {
    peerConnection.addIceCandidate(candidate).then(() => {

    }, () => {
        console.error("failed receiving ice candidate", err)
    });
}

function offerSdp(sdp) {
    socket.send({
        from: username,
        room: room,
        type: 'sdp',
        data: sdp
    })
}