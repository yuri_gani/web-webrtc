var delayOpeningWindow = 0;

var notif_permission = false;


let socket = io('/', {
    autoConnect: false
});
var buddies = {};
var incomingAudio, outgoingAudio;

window.onloadCallback = function(data){
    console.log('onloadCallback', data);
    var container = 'callcs-grecaptcha', params = {
        sitekey: '6Lf78TEUAAAAAFpG7J_d0KjDRfqjVsIXrBltHp83',
        callback: function(resp){
            console.log('grecaptcha callback', resp);
            $('#callcs-grecaptcha').hide();
            socket.open();
        },
        "expired-callback": function(resp){
            console.log('grecaptcha expired-callback', resp);
        }
    };
    grecaptcha.render(container, params);

    // setTimeout(function() {
    //     socket.open();
    // }, 1000);
}
$(() => {
    incomingAudio = new Audio('/ringtones/incoming.mp3');
    outgoingAudio = new Audio('/ringtones/outgoing.ogg');
    incomingAudio.loop = true;
    outgoingAudio.loop = true;
    socket.on("connect", (args) => {
        console.log("connected");
        socket.emit('register', {
            username,
            type: "main",
            session_id: sessionId
        });
    });
    socket.on("disconnect", (args) => {
        console.log("disconnected", args);
    });
    socket.on('register', (arg) => {
        console.log('register', arg);
        if (arg.status) {

            $.get('/callcs/operators', function(respData, resp, req){
                console.log('operators',respData);
                $("tbody#users-list").empty();
                respData.operators.forEach(function (operator, index) {
                    buddies[operator.username] = operator;
                    $(`<tr data-id='` + operator.username + `' class="` + (operator.online ? 'online' : 'offline') + `">
                        <td>` + operator.displayName +(operator.online ? '<img src="/images/online.png" style="float:right;"/>': '<img src="/images/offline.png" style="float:right;"/>')  + `</td>
                    </tr>`).appendTo(this);
                }, $("tbody#users-list"));
            });

            // socket.emit('roster', {});


            if (window.target) {
                call(target, window.media == 'video');
            }
        }
    })
    socket.on("error", (args) => {
        console.log("error", args);
    })
    socket.on("users", (args) => {
        
    });

    socket.on("call", (arg) => {
        arg = Object.assign({
            type: null,
            to: null,
            action: null
        }, arg);
        if (arg.from == username) {
            if (socket.id != arg.from_id) {
                return;
            }
        } else if (arg.to == username) {
            if (typeof arg.to_id === 'string' && socket.id != arg.to_id) {
                showCallEnded(arg);
                endPeerConnection();
                currentCall = null;
                return;
            }
        } else {
            return;
        }

        switch (arg.type) {
            case 'call':
                handleCall(arg);
                break;
            case 'media':
                handleMediaCall(arg);
                break;
            default:
                break;
        }

    });

    socket.on('message', (arg) => {
        console.log("message", arg);
        arg = Object.assign({
            type: null,
            to: null,
            action: null
        }, arg);
        switch (arg.type) {
            case 'call':
                handleCall(arg)
                break;

            default:
                break;
        }

    });

    socket.on('roster', (args) => {
        console.log('roster', args);
        if (Array.isArray(args)) {
            $("tbody#users-list").empty();
            args.forEach(function(user, index) {
                buddies[user.username] = user;
                $(`<tr data-id='` + user.username + `' class="` + (user.online ? 'online' : 'offline') + `">
                    <td>` + user.displayName + `</td>
                    <td><button class="btn btn-xs btn-danger pull-right btn-unsub"><span class="glyphicon glyphicon-remove"></span></button></td>
                </tr>`).appendTo(this);
            }, $("tbody#users-list"));
        } else {
            if (args.type == 'presence') {
                console.log('presence', args);
                var el = $("tbody#users-list").find(`tr[data-id=${args.from}]`);
                el.removeClass('online offline');
                el.addClass(args.status);
                el.find('td:nth-child(1)>img').attr('src', args.status == 'online' ? "/images/online.png": "/images/offline.png");
                if (args.profile) {
                    // el.find('td:first').text(args.profile.displayName);
                    buddies[args.from].displayName = args.profile.displayName;
                    buddies[args.from].profilePicture = args.profile.profilePicture;
                }
            }
        }
    });

    socket.on('subscription', (arg) => {
        console.log('subscription', arg);
        if (arg.type == 'list') {
            var subsContainer = $("tbody#subscribe-list");
            subsContainer.empty();
            arg.data.forEach(function(sub) {
                if (sub.from == username) { // from
                    $(`<tr data-id="` + sub.to + `">
                        <td>` + sub.to + `</td>
                    </tr>`).appendTo(this);
                } else {
                    // $(`<tr data-id="` + sub.from + `">
                    //     <td>` + sub.from + `</td>
                    //     <td>
                    //         <button class="btn btn-xs btn-primary pull-right btn-sub">accept</button>
                    //         <button class="btn btn-xs btn-warning pull-right btn-unsub">reject</button>
                    //     </td>
                    // </tr>`).appendTo(this);
                }
            }, subsContainer);
        }
        
        // else if (arg.type == 'unsubscribe') {
        //     socket.emit('subscription', {
        //         type: 'list'
        //     });
        //     socket.emit('roster', {
        //         type: 'list'
        //     });
        // } else if (arg.type == 'subscribe') {
        //     socket.emit('subscription', {
        //         type: 'list'
        //     });
        //     if (arg.status == 'accepted') {
        //         socket.emit('roster', {
        //             type: 'list'
        //         });
        //     }
        // }
    })

    $("tbody#users-list").on('click', 'tr.online>td', (event) => {
        var to_username = $($(event.target).parents('tr')[0]).attr("data-id");
        showUser(to_username);
    });
    $("#main #call-btn").on('click', (event) => {
        console.log($(event.currentTarget));
        var remoteUser = $(event.currentTarget).parent().parent().attr('remoteUser');
        call(remoteUser);
    })
    $("#main #call-btn-audio").on('click', (event) => {
        console.log($(event.currentTarget));
        var remoteUser = $(event.currentTarget).parent().parent().attr('remoteUser');
        call(remoteUser, false);
    })

    $("#accept-call").on('click', (event) => {
        if (currentCall != null) {
            currentCall.action = "accept";
            socket.send(currentCall);
        }
    })
    $("#reject-call").on('click', (event) => {
        if (currentCall != null) {
            currentCall.action = "reject";
            socket.send(currentCall);
        }
    })
    $("#end-call").on('click', (event) => {
        if (currentCall != null) {
            currentCall.type = "call";
            currentCall.action = "reject";
            socket.send(currentCall);
        }
    })
    $('#calling-modal').on('hidden.bs.modal', (event) => {
        $('#accept-call').show();
    })
    $("#add-friend-btn").on('click', (event) => {
        $('#subscribe-modal').modal('show');
    });
    $('tbody').on('click', '.btn-unsub', function(event) {
        var to_username = $($(event.target).parents('tr')[0]).attr("data-id");
        socket.emit('subscription', {
            type: 'unsubscribe',
            to: to_username
        });
    })
    $('tbody#subscribe-list').on('click', '.btn-sub', function(event) {
        var to_username = $($(event.target).parents('tr')[0]).attr("data-id");
        socket.emit('subscription', {
            type: 'subscribe',
            to: to_username
        });
    })
    $("#modal-subscribe-form").on('submit', (event) => {
        event.preventDefault();
        socket.emit('subscription', {
            type: 'subscribe',
            to: $('#subscribe-to').val()
        });
        $('#subscribe-modal').modal('hide');
        $(event.currentTarget).find("input[type=text], textarea").val("");
    });
    $("#profile-picture").on("click", (event) => {
        $("#profile-picture-modal").modal("show");
    })


    // setTimeout(function() {
    //     socket.open();
    // }, 100);
})


function showUser(remoteUser) {
    var main = $("#main");
    main.attr("remoteUser", remoteUser);
    main.find(".title").text(remoteUser);
    main.find(".title").text(remoteUser);
    main.find("#remote-profile-picture").attr("src", buddies[remoteUser].profilePicture);
    main.show();
}


// --------- CALL ------------
var currentCall = null;
var pc, localStream, remoteStream;
var pcConfig = {
    iceServers: [
        // { urls: 'stun:stun01.sipphone.com' },
        { urls: 'stun:stun.ekiga.net' },
        { urls: 'stun:stun.fwdnet.net' },
        { urls: 'stun:stun.ideasip.com' },
        { urls: 'stun:stun.iptel.org' },
        { urls: 'stun:stun.rixtelecom.se' },
        { urls: 'stun:stun.schlund.de' },
        { urls: 'stun:stun.l.google.com:19305' },
        { urls: 'stun:stun1.l.google.com:19305' },
        { urls: 'stun:stun2.l.google.com:19305' },
        { urls: 'stun:stun3.l.google.com:19305' },
        { urls: 'stun:stun4.l.google.com:19305' },
        { urls: 'stun:stunserver.org' },
        { urls: 'stun:stun.softjoys.com' },
        { urls: 'stun:stun.voiparound.com' },
        { urls: 'stun:stun.voipbuster.com' },
        { urls: 'stun:stun.voipstunt.com' },
        { urls: 'stun:stun.voxgratia.org' },
        { urls: 'stun:stun.xten.com' },
        // {
        //     urls: 'turn:numb.viagenie.ca',
        //     credential: 'muazkh',
        //     username: 'webrtc@live.com'
        // },
        // {
        //     urls: 'turn:192.158.29.39:3478?transport=udp',
        //     credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        //     username: '28224511:1379330808'
        // },
        // {
        //     urls: 'turn:192.158.29.39:3478?transport=tcp',
        //     credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        //     username: '28224511:1379330808'
        // }
    ]
};

function openCallWindow(callObj) {
    window.callObj = callObj;
    var callContainer = $('#incall-modal');
    callContainer.modal('show');
    createPeerConnection();
    setReady();
    // if (currentCall.from == username) {
    //     doOffering();
    // }
}

function handleCall(callObj) {
    console.log('handleCall', callObj);
    if (currentCall != null) {
        if (currentCall.id != callObj.id) {
            if (callObj.action != 'reject') {
                callObj.action = "reject";
                socket.send(callObj);
            }
            return;
        }
    }
    currentCall = callObj;

    switch (callObj.action) {
        case 'calling':
            showIncomingCallDialog(callObj);
            break;
        case 'accept':
            incomingAudio.pause();
            outgoingAudio.pause();
            openCallWindow(callObj);
            break;
        case 'reject':
            incomingAudio.pause();
            outgoingAudio.pause();
            showCallEnded(callObj);
            endPeerConnection();
            currentCall = null;
            break;

        default:
            break;
    }
}

function handleMediaCall(callObj) {
    console.log('handleMediaCall', callObj);
    if (currentCall != null) {
        if (currentCall.id != callObj.id) {
            if (callObj.action != 'reject') {
                callObj.type = "reject";
                callObj.action = "reject";
                socket.send(callObj);
            }
            return;
        }
    }

    switch (callObj.action) {
        case 'ready':
            if (currentCall.from == username)
                doOffering();
            break;
        case 'sdp':
            receiveSdp(JSON.parse(callObj.payload));
            break;
        case 'ice':
            receiveIceCandidate(JSON.parse(callObj.payload));
            break;
        default:
            break;
    }
}

function createPeerConnection() {
    pc = new RTCPeerConnection(pcConfig);
    // pc.ontrack = ontrack;
    pc.onaddstream = onaddstream;
    pc.onicecandidate = onicecandidate;
    pc.oniceconnectionstatechange = oniceconnectionstatechange;
    pc.onicegatheringstatechange = onicegatheringstatechange;
    pc.onnegotiationneeded = onnegotiationneeded;
    pc.onremovestream = onremovestream;
    pc.onsignalingstatechange = onsignalingstatechange;
}

function endPeerConnection() {
    if (localStream && localStream.active) {
        for (var i = 0; i < localStream.getTracks().length; i++) {
            localStream.getTracks()[i].stop();
        }
    }

    if (remoteStream && remoteStream.active) {
        for (var i = 0; i < remoteStream.getTracks().length; i++) {
            remoteStream.getTracks()[i].stop();
        }
    }

    if (pc != null && pc.signalingState != 'closed') {
        pc.close();
    }

    localStream = null;
    remoteStream = null;
    pc = null;
}

function doOffering() {
    console.log("doOffering");
    startMedia((localStream) => {
        pc.addStream(localStream);
        pc.createOffer().then((localSdp) => {

            pc.setLocalDescription(localSdp);
            offerSdp(localSdp);
        }, (err) => {
            console.error("failed do offering", err)
        })
    })
}

function doAnswering(sdp) {
    console.log("doAnswering", sdp);
    pc.setRemoteDescription(sdp)
        .then(data => {
            console.log("setRemotedesc", data);
        })
        .catch(e => console.log(e.name + ": " + e.message));;
    startMedia((localStream) => {
        pc.addStream(localStream);
        pc.createAnswer().then((localSdp) => {
            pc.setLocalDescription(localSdp);
            offerSdp(localSdp);
        }, (err) => {
            console.error("failed do answering", err)
        })
    })

}

function startMedia(fn) {
    if (navigator.getUserMedia) {
        var mediaVideoOpt = {
            video: true,
            audio: true
        };
        var mediaAudioOpt = {
            audio: true,
            video: false
        }
        navigator.getUserMedia(
            window.callObj.media == 'audio' ? mediaAudioOpt : mediaVideoOpt,
            function(stream) {
                localStream = stream;
                // $('#localVideo').get(0).src = window.URL.createObjectURL(localStream);
                var tracks = localStream.getTracks();
                var hasVideo = false;
                for (var index = 0; index < tracks.length; index++) {
                    hasVideo = tracks[index].kind == 'video';
                    if (hasVideo){
                        break;
                    }
                }
                if (hasVideo){
                    $('#localAudio').hide();
                    $('#localVideo').show();
                    $('#localVideo').get(0).srcObject = localStream;
                    
                }else{
                    $('#localVideo').hide();
                    $('#localAudio').show();
                    $('#localAudio').get(0).srcObject = localStream;
                }

                fn(localStream);
            },
            function(error) {
                console.log(JSON.stringify(error));
                console.log(error.message);
            }
        );
    } else {
        alert('Sorry, the browser you are using doesn\'t support getUserMedia');
        return;
    }
}

function setReady() {
    var callSdpObj = Object.assign(currentCall, {
        type: 'media',
        action: 'ready'
    });
    console.log("setReady");
    socket.emit('call', callSdpObj);
}

function offerSdp(sdp) {
    var callSdpObj = Object.assign(currentCall, {
        type: 'media',
        action: 'sdp',
        payload: JSON.stringify(sdp)
    });
    console.log("offerSdp", callSdpObj);
    socket.emit('call', callSdpObj);
}

function receiveSdp(sdp) {
    console.log("sdp", sdp);
    if (currentCall.to == username) {
        doAnswering(sdp);
    } else {
        pc.setRemoteDescription(sdp);
    }
}

function offerIceCandidate(iceCandidate) {
    var callIceCandidateObj = Object.assign(currentCall, {
        type: 'media',
        action: 'ice',
        payload: JSON.stringify(iceCandidate)
    });
    // console.log("offerIceCandidate", callIceCandidateObj);
    socket.emit('call', callIceCandidateObj);
}

function receiveIceCandidate(iceCandidate) {
    console.log("iceCandidate", iceCandidate);
    if (iceCandidate != null){
        pc.addIceCandidate(iceCandidate).then(() => {
            console.log("success receiving ice candidate", iceCandidate);
        }, () => {
            console.error("failed receiving ice candidate", err);
        });
    }
    
}


function onaddstream(event) {
    console.log('onaddstream', event);
    var tracks =  event.stream.getTracks();
    console.log('onaddstream',event, tracks);
    var hasVideo = false;
    for (var index = 0; index < tracks.length; index++) {
        hasVideo = tracks[index].kind == 'video';
        if (hasVideo){
            break;
        }
    }
    remoteStream = event.stream;

    if (hasVideo){
        $('#remoteAudio').hide();
        $('#remoteVideo').show();
        console.log('onaddstream', 'remoteVideo', $('#remoteVideo').get(0));
        $('#remoteVideo').get(0).srcObject = remoteStream;
        // console.log("onaddstream", $('#remoteVideo').get(0).srcObject);
        
    }else{
        $('#remoteVideo').hide();
        $('#remoteAudio').show();
        console.log('onaddstream', 'remoteAudio', $('#remoteAudio').get(0));
        $('#remoteAudio').get(0).srcObject = remoteStream;
        // console.log("onaddstream", $('#remoteAudio').get(0).srcObject);
    }
}

function ontrack(event) {
    console.log('ontrack', event);
}

function onicecandidate(event) {
    if (event.candidate) {
        offerIceCandidate(event.candidate);
    } else {
        console.log('gathering ice candidates finished')
    }
}

function oniceconnectionstatechange(event) {}

function onicegatheringstatechange(event) {}

function onnegotiationneeded(event) {}

function onremovestream(event) {}

function onsignalingstatechange(event) {}


function call(remoteUsername, video) {
    if (typeof video === 'undefined'){
        video = true;
    }

    delayOpeningWindow = 0;
    if (currentCall != null) {
        return;
    }
    currentCall = {
        id: generateUUID(),
        from: username,
        to: remoteUsername,
        type: 'call',
        action: 'calling',
        media: (!!video ? 'video' : 'audio')
    };
    socket.emit('call', currentCall);
}

function showIncomingCallDialog(callObj) {
    var callContainer = $('#calling-modal');
    if (currentCall.from == username) {
        callContainer.find('#accept-call').hide();
        callContainer.find('#calling-remote-name').text(callObj.to);
        outgoingAudio.play()
    } else {
        callContainer.find('#calling-remote-name').text(callObj.from);
        incomingAudio.play()
    }

    callContainer.modal('show');
}

function showCallEnded(callObj) {
    $('#calling-modal').modal('hide');
    $('#incall-modal').modal('hide');
}

// ---------CHAT ---------

function sendChat(to, message) {
    socket.emit('chat', {
        from: username,
        to: to,
        type: 'text',
        data: message,
        state: 'send'
    });
}

function handleChat(chat) {
    console.log(chat);
}

// --------- UTILS -------

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
};