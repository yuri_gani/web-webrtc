console.log("initialize", {
    username,
    room
});

var localVideo;
var remoteVideo;
var debugSection;
var pc = null;
var localStream = null;
var socket;
var turnReady;
var remoteHandshake;

var localHandshakeParam = {
    sdp: null,
    candidates: [],
    candidateFinished: false
};
var pcConfig = {
    'iceServers': [{
        'url': 'stun:stun.l.google.com:19302'
    }, {
        url: 'turn:numb.viagenie.ca',
        credential: 'physic',
        username: 'hyuricane@gmail.com'
    }, ]
};

var callStage = {
    // remote: toUsername,
    // local: username,
    type: 'outgoing',
    state: 'calling'
}

$(() => {
    window.localVideo = localVideo = $("#remoteVideo").get(0);
    localVideo.volume = 0;
    window.remoteVideo = remoteVideo = $("#localVideo").get(0);
    window.debugSection = debugSection = $("#debug-section");

    debugSection.append("hello\n");
    debugSection.append("world\n");


    socket = io('/call');
    socket.on("connect", (args) => {
        console.log("connected");
        debugSection.append("connected\n");

        socket.emit('register', {
            username,
            room
        });
    });
    socket.on("disconnect", (args) => {
        console.log("disconnected");
        debugSection.append("disconnected\n");
    });

    socket.on('message', (arg) => {});
    socket.on('offer', function() {
        createPeerConnection();
    })
    socket.on('answer');

    socket.on('handshake', (handshakeData) => {
        console.log('handshake', handshakeData);
        debugSection.append("handshake\n");
        for (var remote in handshakeData) {
            if (remote != username && handshakeData.hasOwnProperty(remote)) {
                if (!remoteHandshake && handshakeData[remote].sdp) {
                    remoteHandshake = handshakeData[remote];
                    console.log(remoteHandshake);
                    if (pc == null) { // receiver
                        createPeerConnection()
                    } else {
                        acceptHandshake();
                        // pc.setRemoteDescription(new RTCSessionDescription(remoteHandshake.sdp));
                    }
                }

            }
        }
    });

    requestTurn(
        'https://computeengineondemand.appspot.com/turn?username=41784574&key=4080218913'
    );

    startMedia();
})

function handleCall(callObj) {
    console.log("handleCall");
    debugSection.append("handleCall\n");
    switch (callObj.action) {
        case 'accept':

            break;
        case 'hangup':

        case 'reject':

            break;

        default:
            break;
    }
}

function startMedia() {
    console.log("startMedia");
    debugSection.append("startMedia\n");
    navigator.getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    if (navigator.getUserMedia) {
        navigator.getUserMedia({
                video: true,
                audio: true
            },
            function(stream) {
                console.log("streaming");
                debugSection.append("streaming\n");
                localVideo.src = window.URL.createObjectURL(stream);
                localVideo.onloadedmetadata = function(e) {
                    console.log("playing");
                };
                localStream = stream;
                // createPeerConnection();
                addLocalStreamToPeerConnection();
            },
            function(error) {
                console.err('startMedia', error)
                debugSection.append("startMedia error\n");
            }
        );
    } else {
        alert('Sorry, the browser you are using doesn\'t support getUserMedia');
        return;
    }
}

function createPeerConnection() {
    console.log("createPeerConnection");
    debugSection.append("createPeerConnection\n");
    try {
        pc = new RTCPeerConnection(null);
        pc.onicecandidate = handleIceCandidate;
        pc.onaddstream = handleRemoteStreamAdded;
        pc.onremovestream = handleRemoteStreamRemoved;
        console.log('Created RTCPeerConnnection');
        debugSection.append("Created RTCPeerConnnection\n");
    } catch (e) {
        console.log('Failed to create PeerConnection, exception: ' + e.message);
        alert('Cannot create RTCPeerConnection object.');
        return;
    }
    addLocalStreamToPeerConnection();
}

function addLocalStreamToPeerConnection() {
    if (pc == null) {
        // console.error("peer is null");
        debugSection.append("peer is null\n");
        return;
    }
    if (localStream == null) {
        // console.error("video not ready");
        debugSection.append("video not ready\n");
        return;
    }
    console.log("addLocalStreamToPeerConnection");
    debugSection.append("addLocalStreamToPeerConnection\n");
    pc.addStream(localStream);
    if (remoteHandshake) {
        acceptHandshake();
        createAnswer();
    } else {
        createOffer()
    }
}

function addRemoteIceCandidates() {
    remoteHandshake.candidates.forEach(function(element) {
        pc.addIceCandidate({

        });

    }, this);
}

function createOffer() {
    if (pc != null) {
        console.log("createOffer");
        debugSection.append("createOffer\n");
        pc.createOffer((sdp) => {
            console.log("sdp offer created", sdp);
            debugSection.append("sdp offer created\n");
            pc.setLocalDescription(sdp);
            localHandshakeParam.sdp = sdp;
            if (localHandshakeParam.candidateFinished) {
                debugSection.append("send handshake\n");
                socket.emit("handshake", localHandshakeParam);
            }

        }, (err) => {
            console.log("error creating sdp offer", err);
            debugSection.append("error creating sdp offer\n");
        });
    }

}

function createAnswer() {
    if (pc != null) {
        console.log("createAnswer");
        debugSection.append("createAnswer\n");
        pc.createAnswer((sdp) => {
            console.log("sdp answer created", sdp);
            debugSection.append("sdp answer created\n");
            pc.setLocalDescription(sdp);
            localHandshakeParam.sdp = sdp;
            if (localHandshakeParam.candidateFinished) {
                debugSection.append("send handshake\n");
                socket.emit("handshake", localHandshakeParam);
            }
        }, (err) => {
            console.log("error creating sdp answer", err);
            debugSection.append("error creating sdp answer\n");
        });
    }
}

function acceptHandshake() {
    console.log("acceptHandshake");
    debugSection.append("acceptHandshake\n");
    for (var i = 0; i < remoteHandshake.candidates.length; i++) {
        var iceCandidate = remoteHandshake.candidates[i];
        pc.addIceCandidate(new RTCIceCandidate({
            sdpMLineIndex: iceCandidate.label,
            candidate: iceCandidate.candidate
        }));
    }
    pc.setRemoteDescription(new RTCSessionDescription(remoteHandshake.sdp));
}

function handleIceCandidate(event) {
    console.log('handleIceCandidate', event);
    debugSection.append("handleIceCandidate\n");
    if (event.candidate) {
        localHandshakeParam.candidates.push({
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate
        });

    } else {
        console.log('End of candidates.');
        localHandshakeParam.candidateFinished = true;
        if (localHandshakeParam.sdp) {
            socket.emit("handshake", localHandshakeParam);
        }
    }
}

function handleRemoteStreamAdded(event) {
    console.log('handleRemoteStreamAdded');
    debugSection.append("handleRemoteStreamAdded\n");
    remoteStream = event.stream;
    remoteVideo.srcObject = event.stream;
    // remoteVideo.play();
}

function handleRemoteStreamRemoved(event) {
    console.log('handleRemoteStreamRemoved', event);
    debugSection.append("handleRemoteStreamRemoved\n");
}


function requestTurn(turnURL) {
    console.log("requestTurn", turnURL);
    debugSection.append("requestTurn\n");
    var turnExists = false;
    for (var i in pcConfig.iceServers) {
        if (pcConfig.iceServers[i].url.substr(0, 5) === 'turn:') {
            turnExists = true;
            turnReady = true;
            break;
        }
    }
}