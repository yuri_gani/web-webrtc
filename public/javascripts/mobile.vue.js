let statusText;

var app, pc, localVideo;
var pcConfig = {
    'iceServers': [
        { urls: 'stun:stun.ekiga.net' },
        { urls: 'stun:stun.fwdnet.net' },
        { urls: 'stun:stun.ideasip.com' },
        { urls: 'stun:stun.iptel.org' },
        { urls: 'stun:stun.rixtelecom.se' },
        { urls: 'stun:stun.schlund.de' },
        { urls: 'stun:stun.l.google.com:19305' },
        { urls: 'stun:stun1.l.google.com:19305' },
        { urls: 'stun:stun2.l.google.com:19305' },
        { urls: 'stun:stun3.l.google.com:19305' },
        { urls: 'stun:stun4.l.google.com:19305' },
        { urls: 'stun:stunserver.org' },
        { urls: 'stun:stun.softjoys.com' },
        { urls: 'stun:stun.voiparound.com' },
        { urls: 'stun:stun.voipbuster.com' },
        { urls: 'stun:stun.voipstunt.com' },
        { urls: 'stun:stun.voxgratia.org' },
        { urls: 'stun:stun.xten.com' },
    ]
};

if (typeof window.handshake === 'object') {

    handshake.onRemoteReady = function() {
        if (app) {
            app.onRemoteReady();
        }
    }

    handshake.onSdpReceived = function(sdpStr) {
        console.log("onSdpReceived");
        var sdp = JSON.parse(sdpStr);
        if (app) {
            app.receiveSdp(sdp);
        }
    }

    handshake.oniceCandidateReceived = function(iceCandidateStr) {
        console.log("oniceCandidateReceived" + iceCandidateStr);
        var iceCandidate = JSON.parse(iceCandidateStr);
        if (iceCandidate != null) {
            app.receiveIceCandidate(iceCandidate);
            // pc.addIceCandidate(iceCandidate).then(() => {
            //     console.log("success receiving ice candidate", iceCandidate);
            // }, (err) => {
            //     console.error("failed receiving ice candidate", err);
            // });
        }
    }

    handshake.onClose = function(){
        pc.close();
        app.close();
    }

    handshake.onpauseSendingVideo = function(sdp) {

    }

    handshake.onresumeSendingVideo = function(sdp) {

    }



    handshake.addListener('onRemoteReady', 'handshake.onRemoteReady');
    handshake.addListener('sdpReceived', 'handshake.onSdpReceived');
    handshake.addListener('iceCandidateReceived', 'handshake.oniceCandidateReceived');
    handshake.addListener('pauseSendingVideo', 'handshake.onpauseSendingVideo');
    handshake.addListener('resumeSendingVideo', 'handshake.onresumeSendingVideo');
    handshake.addListener('onClose', 'handshake.onClose');
} else {
    // window.location = "/";
}


window.addEventListener('load', () => {


    // Vue.use(VueMaterial);

    statusText = new Vue({
        el: '#console-repl',
        data: function(params) {
            return {
                status: "init"
            }
        },
        methods: {
            setText: function(text) {
                this.status = text;
            }
        }
    });

    Vue.component('localvideo', {
        template: `<video ref="self" autoplay="true" muted="muted" v-show="useVideo && stream"></video>`,
        data: function() {
            return {
                stream: null,
                useVideo: (handshake || false) && handshake.useVideo()
            }
        },
        methods: {
            loadStream: function(stream) {
                if (this.stream != stream) {
                    statusText.setText("load local Stream");
                    this.stream = stream;
                    this.$refs.self.srcObject = stream;
                }
            },
            startMedia: function(fn) {
                if (navigator.getUserMedia) {
                    statusText.setText("user media available");
                    var mediaVideoOpt = {
                        video: true,
                        audio: true
                    };
                    var mediaAudioOpt = {
                        audio: true,
                        video: false
                    }
                    var media2Use = (handshake || false) && !handshake.useVideo() ? mediaAudioOpt : mediaVideoOpt;
                    navigator.getUserMedia(
                        media2Use,
                        fn,
                        (error) => {
                            statusText.setText("media error");
                            console.log(JSON.stringify(error));
                            console.log(error.message);
                        }
                    );
                } else {
                    statusText.setText("user media unavailable");
                    alert('Sorry, the browser you are using doesn\'t support getUserMedia');
                    return;
                }
            },
            getStream: function() {
                return this.stream;
            },
            close: function(){
                this.stream.close();
            }
        },
    });

    Vue.component('remotevideo', {
        template: `<video ref="self1" autoplay="true" v-show="stream" :poster="poster" controls></video>`,
        data: function() {
            return {
                stream: null,
                poster: (handshake || false) ? handshake.getProfilePicture() : null
            }
        },
        methods: {
            loadStream: function(stream) {
                console.log("load remote strream");
                statusText.setText("load remote Stream");
                this.stream = stream;
                this.$refs.self1.srcObject = stream;
                this.$refs.self1.volume = 1;
            },
            getStream: function() {
                return this.stream;
            },
            close: function(){
                this.stream.close();
            }
        }

    });

    app = new Vue({
        el: "#app",
        data: function() {
            return {
                username: ""
            };
        },
        created: function() {
            console.log("created");
            statusText.setText("created");
            createPeerConnection();
            if (typeof handshake === 'object') {
                handshake.setReady();
            }
        },
        mounted: function() {
            console.log('mounted', this.$refs.localvideo);
        },
        methods: {
            setRemoteStream: function(stream) {
                console.log("setRemoteStream", stream);
                this.$refs.remotevideo.loadStream(stream);
            },
            onRemoteReady: function() {
                if (typeof handshake === 'object') {
                    if (handshake.isOffering()) {
                        this.$refs.localvideo.startMedia((stream) => {
                            this.$refs.localvideo.loadStream(stream);
                            pc.addStream(stream);
                            pc.createOffer().then((sdp) => {
                                console.log('sdp offer created');
                                var sdpString = JSON.stringify(sdp)
                                console.log(sdpString);
                                pc.setLocalDescription(sdp);
                                handshake.onSdpCreated(sdpString);
                            }).catch((error) => {
                                console.error('failed creating sdp', error);
                                console.error(error);
                            });
                        })
                    } else {
                        // wait for remote sdp
                    }
                }
            },
            receiveSdp: function(sdp) {
                if (typeof handshake === 'object') {
                    pc.setRemoteDescription(sdp);
                    if (!handshake.isOffering()) {
                        this.$refs.localvideo.startMedia((stream) => {
                            this.$refs.localvideo.loadStream(stream);
                            pc.addStream(stream);
                            pc.createAnswer().then((localSdp) => {
                                console.log('sdp offer created');
                                var sdpString = JSON.stringify(localSdp)
                                console.log(sdpString);

                                pc.setLocalDescription(localSdp);
                                handshake.onSdpCreated(sdpString);
                            }).catch((error) => {
                                console.error('failed creating sdp', error);
                                console.error(error);
                            });
                        });
                    }
                }
            },
            receiveIceCandidate: function(iceCandidate) {
                pc.addIceCandidate(iceCandidate).then(() => {
                    console.log("success receiving ice candidate");
                    console.log(iceCandidate);
                }, (err) => {
                    console.error("failed receiving ice candidate");
                    console.error(err);
                });
            },
            close: function(){
                this.$refs.remotevideo.close();
                this.$refs.localvideo.close();
            }
        }
    });



});



function createPeerConnection() {
    pc = new RTCPeerConnection(pcConfig);
    // pc.ontrack = ontrack;
    pc.onaddstream = onaddstream;
    pc.onicecandidate = onicecandidate;
    pc.oniceconnectionstatechange = oniceconnectionstatechange;
    pc.onicegatheringstatechange = onicegatheringstatechange;
    pc.onnegotiationneeded = onnegotiationneeded;
    pc.onremovestream = onremovestream;
    pc.onsignalingstatechange = onsignalingstatechange;
}

function startMedia(fn) {
    if (navigator.getUserMedia) {
        statusText.setText("user media available");
        var mediaVideoOpt = {
            video: true,
            audio: true
        };
        var mediaAudioOpt = {
            audio: true,
            video: false
        }
        navigator.getUserMedia(
            mediaVideoOpt,
            fn,
            function(error) {
                statusText.setText("media error");
                console.log(JSON.stringify(error));
                console.log(error.message);
            }
        );
    } else {
        statusText.setText("user media unavailable");
        alert('Sorry, the browser you are using doesn\'t support getUserMedia');
        return;
    }
}

function sdpListener(sdp) {
    if (typeof statusText !== 'undefined') {
        statusText.setText(sdp);
    }
}


function onaddstream(event) {
    console.log('onaddstream');
    console.log(event.stream);
    if (typeof app !== 'undefined') {
        app.setRemoteStream(event.stream);                                                                                                                                                      
    }
}

function onremovestream(event) {

}

function onicecandidate(event) {
    console.log('onIceCandidate', event.candidate);
    if (typeof handshake !== 'undefined') {
        handshake.onIceCandidateOffered(JSON.stringify(event.candidate));
    }
}

function oniceconnectionstatechange(event) {

}

function onicegatheringstatechange(event) {

}

function onnegotiationneeded(event) {

}

function onsignalingstatechange(event) {

}