$(() => {
    $("#add-user-panel").on("hide.bs.modal", function(e) {
        $(e.currentTarget).find("input[type=text], input[type=password]").val("");
    });
    $("#add-user-btn").on("click", function(event) {
        event.preventDefault();
        $("#add-user-panel").modal("show");
    });
    $("form#add-user-form").on("submit", function(e) {
        e.preventDefault();
        var username = $("form input[name=username]").val();
        var password = $("form input[name=password]").val();
        var displayName = $("form input[name=displayName]").val();
        var data = {
            username,
            password,
            displayName
        };
        $.ajax({
            url: '/admin/users',
            type: 'post',
            data: data,
            success: function(response) {
                $("#add-user-panel").modal("hide");
                window.location.reload();
            },
            failed: function(err, response) {
                console.err(err, response);
            }
        })
    });
    $(document).on("click", ".btn-delete", function(event) {
        var elem = $(event.currentTarget);
        console.log(event.currentTarget);
        $.ajax({
            url: '/admin/users/' + elem.attr('data-id'),
            type: 'delete',
            success: function(response) {
                window.location.reload();
            },
            failed: function(err, response) {
                console.err(err, response);
            }
        })
    });
    $.ajax({
        url: '/admin/users',
        type: 'GET',
        success: function(response) {
            response.users.forEach(function(user, i) {
                console.log(user);
                $('<tr>' +
                    '<td>' + user.username + '</td>' +
                    '<td>' + user.displayName + '</td>' +
                    '<td>' + (user.type || 'local')  + '</td>' +
                    '<td><button class="btn btn-warning btn-delete" data-id="' + user._id + '" >Delete</button></td>' +
                    '</tr>').appendTo("#users");
                $('#users')
            }, this);
        },
        failed: function(error, response) {

        }
    });
});