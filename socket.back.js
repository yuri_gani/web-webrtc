const socketIO = require('socket.io');
const uuidV4 = require('uuid/v4');
const fs = require('fs');
var crypto = require('crypto');
var mime = require('mime');
const fileType = require('file-type');
const User = require('./model/User');
const Subscription = require('./model/Subscription');
const Roster = require('./model/Roster');

const profilePictureDir = 'public/images/profilepicture/';

function socketMain(serverHttps) {
    var io = socketIO(serverHttps);

    var connections = {};

    var rooms = {

    }

    function connectionsToArray(user) {
        if (typeof user === 'string') {
            // User.findOne({
            //     username: user
            // }).populate('roster').then((user) => {
            //     if (user) {
            //         console.log(user.roster.users);
            //     }
            // })
        } else if (typeof user === 'object') {
            if (user._id) {

            } else {

            }
        } else {
            var retval = Object.keys(connections);
            return retval;
        }
    }

    function deleteSocketId(socketId) {
        var isMain = false;
        for (var username in connections) {
            if (connections.hasOwnProperty(username)) {
                // console.log("check username", username);
                for (var id in connections[username]) {
                    // console.log("check socket.id", id);

                    if (connections[username].hasOwnProperty(id)) {
                        if (id == socketId) {
                            isMain = connections[username][id];
                            delete connections[username][id];
                            console.log("delete", socketId);
                        }
                    }
                }
                if (Object.keys(connections[username]).length == 0) {
                    delete connections[username];
                }
            }
        }
        console.log("connections", connections);
        return isMain;
    }


    const ioMain = io;
    // const ioMain = io.of('/main');
    // const ioCall = io.of('/call');
    ioMain.on("connection", (socket) => {
        console.log(socket.id, "from", socket.conn.remoteAddress, "connected");

        socket.on("disconnect", () => {
            console.log(socket.id, "disconnected");
            if (deleteSocketId(socket.id))
                ioMain.emit('users', connectionsToArray());

        });
        socket.on("error", () => {
            console.log("error", error);
            if (deleteSocketId(socket.id))
                ioMain.emit('users', connectionsToArray());
        });
        socket.on("register", (arg) => {
            arg = Object.assign({
                username: null,
                type: 'main'
            }, arg);

            if (!connections.hasOwnProperty(arg.username)) {
                connections[arg.username] = {};
            }

            if (arg.password) { // do login
                User.findOne({
                    username: arg.username
                }).select('+password').populate('roster').then(function(user) {
                    if (user) {
                        user.comparePassword(arg.password, function(err, isMatch) {
                            if (err)
                                console.error(err);
                            if (isMatch) {
                                console.log('register', arg.username);

                                socket.join('user#' + arg.username);
                                connections[arg.username][socket.id] = true;
                                socket.emit("register", {
                                    status: true,
                                    profile: {
                                        username: arg.username,
                                        displayName: user.displayName,
                                        profilePicture: user.profilePicture
                                    }
                                });
                                // ioMain.emit('users', connectionsToArray());
                                console.log(user.roster.users);
                            } else {
                                socket.emit("register", {
                                    status: false
                                });
                                socket.disconnect();
                            }
                        })
                    } else {
                        socket.emit("register", {
                            status: false
                        });
                        socket.disconnect();
                    }
                }).catch(function(err) {
                    console.error(err);
                    socket.emit("register", {
                        status: false
                    });
                    socket.disconnect();
                })
            } else if (arg.session_id) { // check session id
                sessionStore.get(arg.session_id, function(err, data) {
                    if (err) {
                        console.error(err);
                    } else if (arg.username == data.username && data.authenticated) {
                        console.log('register', arg.username);

                        socket.join('user#' + arg.username);
                        connections[arg.username][socket.id] = true;
                        // ioMain.emit('users', connectionsToArray());
                        User.findOne({
                            username: arg.username
                        }).populate('roster').then((user) => {
                            if (user) {
                                socket.emit('register', {
                                    status: true
                                });
                            }
                            // console.log(user);
                            // socket.emit('roster', user.roster.users);
                        })
                    } else {
                        socket.emit("register", {
                            status: false
                        });
                        socket.disconnect();
                    }

                });
            } else {
                socket.emit("register", {
                    status: false
                });
                socket.disconnect();
            }


        });
        socket.on("users", (args) => {
            socket.emit("users", Object.keys(connections));
        });


        socket.on("roster", (arg) => {
            var username = getSocketUsername(socket);

            User.findOne({
                username: username
            }).populate({
                path: 'roster',
                populate: {
                    path: 'users',
                }
            }).lean().then((user) => {
                // console.log('roster', username, user.roster.users);
                var users = [];
                for (var i = 0; i < user.roster.users.length; i++) {
                    var u = user.roster.users[i];
                    users.push({
                        _id: u._id,
                        username: u.username,
                        displayName: u.displayName,
                        profilePicture: u.profilePicture,
                        online: !!connections[u.username]
                    });
                }
                console.log(users);
                setTimeout(function() {
                    socket.emit("roster", users);
                }, 1000);
            }).catch((err) => {
                console.error(err);
            });
        })


        socket.on('message', (arg) => {
            console.log('message', arg);
            arg = Object.assign({
                id: uuidV4(),
                type: null,
                to: null,
                action: null
            }, arg);

            switch (arg.type) {
                case 'call':
                    processCall(arg, socket);
                    break;
                default:
                    break;
            }

        });

        socket.on('call', (arg) => {
            arg = Object.assign({
                type: 'call', // 'call', 'sdp', 'ice'
                to: null,
                action: null
            }, arg);

            processCall(arg, socket);

        })

        // socket.on('chat', (arg) => {
        //     arg = Object.assign({
        //         id: uuidV4(),
        //         from: getSocketUsername(socket),
        //         to: null,
        //         type: null,
        //         data: null,
        //         state: null,
        //         time: (new Date()).getTime()
        //     }, arg);
        //     processChat(arg, socket);
        // })

        socket.on('subscription', (arg) => {
            console.log('subscription', arg);
            User.findOne({
                    username: getSocketUsername(socket)
                })
                .populate({
                    path: 'roster',
                    populate: {
                        path: 'subscriptions'
                    }
                })
                .then((from) => {
                    if (from) {
                        if (arg.type == 'subscribe' || arg.type == 'unsubscribe') {
                            User.findOne({
                                    username: arg.to
                                })
                                .populate("roster")
                                .then((to) => {
                                    if (arg.type == 'subscribe') { // subscribe
                                        // find if already subscribed
                                        // search in roster
                                        var foundInRoster = false;
                                        for (var i = 0; i < from.roster.users.length; i++) {
                                            var rUser = from.roster.users[i];
                                            if (rUser.equals(to._id)) {
                                                foundInRoster = true;
                                                break;
                                            }
                                        }
                                        if (foundInRoster) { // already accepted
                                            console.log(from.username, "found", to.username, "in roster");
                                            socket.emit('subscription', {
                                                type: 'subscribe',
                                                from: from.username,
                                                to: to.username,
                                                status: 'accepted'
                                            })
                                            return;
                                        }
                                        // search in subscription
                                        var foundInSubscription = false;
                                        for (var i = 0; i < from.roster.subscriptions.length; i++) {
                                            var rSub = from.roster.subscriptions[i];
                                            if (rSub.to.equals(to._id)) { // already exists
                                                socket.emit('subscription', {
                                                    type: 'subscribe',
                                                    from: from.username,
                                                    to: to.username,
                                                    status: 'pending'
                                                })
                                                foundInSubscription = true;
                                                break;
                                            } else if (rSub.from.equals(to._id)) { // accept subscription
                                                rSub.status = 'accepted';
                                                rSub.save().then(() => {
                                                    from.roster.users.push(to._id);
                                                    to.roster.users.push(from._id);
                                                    from.roster.save();
                                                    to.roster.save();
                                                    var sub = {
                                                        type: 'subscribe',
                                                        from: from.username,
                                                        to: to.username,
                                                        status: 'accepted'
                                                    };
                                                    socket.to('user#' + arg.to).emit('subscription', sub);
                                                    socket.emit('subscription', sub);
                                                }).catch((err) => {
                                                    console.error(err);
                                                });
                                                foundInSubscription = true;
                                                break;
                                            }
                                        }
                                        if (!foundInSubscription) {
                                            // create new
                                            var newSub = new Subscription({
                                                from: from._id,
                                                to: to._id
                                            });
                                            newSub.save().then((sub) => {
                                                from.roster.subscriptions.push(sub._id);
                                                to.roster.subscriptions.push(sub._id);
                                                from.roster.save();
                                                to.roster.save();
                                                var sub = {
                                                    type: 'subscribe',
                                                    from: from.username,
                                                    to: to.username,
                                                    status: 'pending'
                                                };
                                                socket.to('user#' + arg.to).emit('subscription', sub);
                                                socket.emit('subscription', sub);
                                            });
                                        }


                                    } else { // unsubscribe
                                        // search in roster
                                        var foundInRoster = false;
                                        for (var i = 0; i < from.roster.users.length; i++) {
                                            var rUser = from.roster.users[i];
                                            if (rUser.equals(to._id)) {
                                                from.roster.users.splice(i, 1);
                                                foundInRoster = true;
                                                break;
                                            }
                                        }
                                        from.roster.save().then(() => {
                                            console.log('delete user', to.username, 'from', from.username, `'s roster`);
                                        });

                                        if (foundInRoster) {
                                            for (var i = 0; i < to.roster.users.length; i++) {
                                                var rUser = to.roster.users[i];
                                                if (rUser.equals(from._id)) {
                                                    to.roster.users.splice(i, 1);
                                                    break;
                                                }
                                            }
                                            to.roster.save().then(() => {
                                                console.log('delete user', from.username, 'from', to.username, `'s roster`);
                                            });
                                            var sub = {
                                                type: 'unsubscribe',
                                                from: from.username,
                                                to: to.username
                                            }
                                        }
                                        Subscription.remove({
                                            $or: [{
                                                    from: from._id,
                                                    to: to._id
                                                },
                                                {
                                                    to: from._id,
                                                    from: to._id
                                                },
                                            ]
                                        }).then(() => {
                                            var sub = {
                                                type: 'unsubscribe',
                                                from: from.username,
                                                to: to.username
                                            }
                                            socket.to('user#' + arg.to).emit('subscription', sub);
                                            socket.emit('subscription', sub);
                                        }).catch((err) => {
                                            console.error(err);
                                        });
                                    }

                                }).catch((err) => {
                                    throw err;
                                });
                        } else if (arg.type == 'list') {
                            Subscription.find({
                                    $and: [{
                                        status: 'pending'
                                    }, {
                                        $or: [{
                                                from: from._id,
                                                status: 'pending'
                                            },
                                            {
                                                to: from._id,
                                                status: 'pending'
                                            },
                                        ]
                                    }],
                                })
                                .populate('from')
                                .populate('to')
                                .lean()
                                .then((subMs) => {
                                    var subs = [];
                                    for (var index = 0; index < subMs.length; index++) {
                                        var subM = subMs[index];
                                        subs.push({
                                            from: subM.from.username,
                                            to: subM.to.username,
                                            status: subM.status
                                        });
                                    }
                                    socket.emit('subscription', {
                                        type: 'list',
                                        data: subs
                                    });
                                }).catch((err) => {
                                    console.error(err);
                                });
                        }

                    }
                }).catch((err) => {
                    throw err;
                })
        });

        socket.on('profilePicture', (arg) => {
            User.findOne({
                username: getSocketUsername(socket)
            }).then((user) => {
                if (Buffer.isBuffer(arg)) {
                    var ft = fileType(arg);
                    if (ft.mime.startsWith('image')) {
                        var tempName = profilePictureDir + 'temp-' + Date.now();
                        console.log('profilePicture', 'tempName', tempName);
                        fs.writeFile(tempName, arg, (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            crypto.pseudoRandomBytes(16, function(err, raw) {
                                if (err) {
                                    console.error(err);
                                    return;
                                }
                                var fn = profilePictureDir + Date.now() + '-' + raw.toString('hex') + '.' + ft.ext;

                                console.log('profilePicture', 'newName', fn);
                                fs.rename(tempName, fn, (err) => {
                                    if (err) {
                                        console.error(err);
                                        return;
                                    }

                                    var newProfilePicturePath = fn.replace("public/", "/");
                                    user.profilePicture = newProfilePicturePath;
                                    user.save().then((doc) => {
                                        console.log("update user", doc);
                                        socket.emit('profilePicture', {
                                            status: true,
                                            data: newProfilePicturePath
                                        });
                                    }).catch((err) => {
                                        console.error(err);
                                    });
                                });

                            });
                        });
                    }
                } else if (typeof arg === 'string') {
                    console.log("profilePicture", "unsupported");
                } else {

                }
            }).catch((err) => {
                console.error(err);
            });
        })

        socket.on("displayName", (arg) => {
            User.update({
                username: getSocketUsername(socket)
            }, {
                displayName: arg
            }).then((doc) => {
                socket.emit('displayName', arg);
            }).catch((err) => {
                console.error(err);
            });
        });
    })

    function processCall(call, socket) {
        console.log('processCall', call);
        console.log('from', connections[call.from]);
        console.log('to', connections[call.to]);

        let username = getSocketUsername(socket);
        call.from = call.from || username;
        if (call.from == call.to) {

        } else if (connections[call.to] && connections[call.from]) {
            let target = call.to;
            if (username == call.from) {
                target = call.to;
            } else {
                target = call.from;
            }

            if (call.type == 'call') {
                if (call.action == 'calling') {
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                } else if (call.action == 'accept') {
                    if (username == call.from) return;
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                } else if (call.action == 'reject') {
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                }
            } else if (call.type == 'media') {
                if (call.action == 'ready') {
                    socket.to('user#' + target).emit('call', call);
                    return;
                } else if (call.action == 'sdp') {
                    console.log("send sdp to: ", target, " : ", call);
                    socket.to('user#' + target).emit('call', call);
                    return;
                } else if (call.action == 'ice') {
                    console.log("send ice to: ", target, " : ", call);
                    socket.to('user#' + target).emit('call', call);
                    return;
                }
            }
        } else {
            call.action = 'unavailable';
            socket.emit('call', call);
            return;
        }
        console.log('processCall', call, 'invalid');
        call.action = 'invalid';
        socket.emit('call', call);
    }

    function processChat(chat, socket) {
        var username = getSocketUsername(socket);
        var target = null;
        if (username == socket.from) { // regular
            target = chat.to;
            if (connections[target]) {
                socket.to("user#" + target).emit('chat', chat);
            } else {
                storeChat(chat, username, target);
            }
        } else { // status | ack
            target = username;
            if (connections[target]) {
                socket.to("user#" + target).emit('chat', chat);
            } else {
                storeChat(chat, username, target);
            }
        }
    }

    function storeChat(chat, username, target) {
        // TODO: implement store and send
    }

    function getSocketUsername(socket) {
        for (var room in socket.rooms) {
            if (socket.rooms.hasOwnProperty(room)) {
                if (room.startsWith('user#')) {
                    return room.substr(5);
                }
            }
        }
        return null;
    }
}



module.exports = socketMain;