const socketIO = require('socket.io');
const uuidV4 = require('uuid/v4');
const fs = require('fs');
var crypto = require('crypto');
var mime = require('mime');
const fileType = require('file-type');
const User = require('./model/User');
const Subscription = require('./model/Subscription');
const Roster = require('./model/Roster');

const profilePictureDir = 'public/images/profilepicture/';

function socketMain(serverHttps) {
    var io = socketIO(serverHttps);
    const ioMain = io;

    var connections = {};

    var rooms = {

    }

    function connectionsToArray(user) {
        if (typeof user === 'string') {

        } else if (typeof user === 'object') {
            if (user._id) {

            } else {

            }
        } else {
            var retval = Object.keys(connections);
            return retval;
        }
    }

    function deleteSocketId(socketId) {
        var isMain = false;
        for (var username in connections) {
            if (connections.hasOwnProperty(username)) {
                for (var index = connections[username].sockets.length - 1; index >= 0; index--) {
                    if (connections[username].sockets[index] == socketId) {
                        connections[username].sockets.splice(index, 1);
                        if (connections[username].sockets.length == 0) {
                            delete connections[username];

                            // send presence offline to subscribers
                            emitPresence(username, 'offline');
                            break;
                        }
                    }
                }
            }
        }
        return isMain;
    }

    function emitPresence(username, status, profile) {
        ioMain.to("subs#" + username).emit('roster', {
            type: 'presence',
            from: username,
            status: status,
            profile
        });
    }

    function register(socket, user) {
        if (!connections.hasOwnProperty(user.username)) {
            connections[user.username] = {
                presence: 'online',
                sockets: []
            };
        }
        console.log('register', user.username);
        socket.join('user#' + user.username);
        connections[user.username].sockets.push(socket.id);
        connections[user.username].profile = {
            username: user.username,
            displayName: user.displayName,
            profilePicture: user.profilePicture
        };
        socket.emit("register", {
            status: true,
            profile: connections[user.username].profile
        });
        User.find({
            '_id': {
                $in: user.roster.users
            }
        }).lean().then((subTos) => {
            for (var index = 0; index < subTos.length; index++) {
                var subTo = subTos[index];
                socket.join('subs#' + subTo.username);
            }
        }).catch((err) => {
            console.error(err);
        });
    }



    // const ioMain = io.of('/main');
    // const ioCall = io.of('/call');
    ioMain.on("connection", (socket) => {
        console.log(socket.id, "from", socket.conn.remoteAddress, "connected");

        socket.on("disconnect", () => {
            console.log(socket.id, "disconnected");
            deleteSocketId(socket.id);

        });
        socket.on("error", () => {
            console.log("error", error);
            deleteSocketId(socket.id);
        });
        socket.on("register", (arg) => {
            arg = Object.assign({
                username: null,
                type: 'main'
            }, arg);

            if (arg.password) { // do login
                User.findOne({
                    username: arg.username
                }).select('+password').populate('roster').then(function(user) {
                    if (user) {
                        user.comparePassword(arg.password, function(err, isMatch) {
                            if (err)
                                console.error(err);
                            if (isMatch) {
                                // add to connections
                                register(socket, user);
                                emitPresence(user.username, 'online', connections[user.username].profile);
                            } else {
                                socket.emit("register", {
                                    status: false
                                });
                                socket.disconnect();
                            }
                        })
                    } else {
                        socket.emit("register", {
                            status: false
                        });
                        socket.disconnect();
                    }
                }).catch(function(err) {
                    console.error(err);
                    socket.emit("register", {
                        status: false
                    });
                    socket.disconnect();
                })
            } else if (arg.session_id) { // check session id
                sessionStore.get(arg.session_id, function(err, data) {
                    if (err) {
                        console.error(err);
                    } else if (arg.username == data.username && data.authenticated) {
                        User.findOne({
                            username: arg.username
                        }).populate('roster').then((user) => {
                            if (user) {
                                register(socket, user);
                                emitPresence(user.username, 'online', connections[user.username].profile);
                            } else {
                                socket.emit("register", {
                                    status: false
                                });
                                socket.disconnect();
                            }
                        })
                    } else {
                        socket.emit("register", {
                            status: false
                        });
                        socket.disconnect();
                    }

                });
            } else {
                socket.emit("register", {
                    status: false
                });
                socket.disconnect();
            }


        });
        socket.on("users", (args) => {
            var username = getSocketUsername(socket);

            socket.emit("users", Object.keys(connections));
        });


        socket.on("roster", (arg) => {
            arg = Object.assign({
                type: 'query',
            }, arg);
            var username = getSocketUsername(socket);
            console.log('roster', arg);

            User.findOne({
                username: username
            }).populate({
                path: 'roster',
                populate: {
                    path: 'subscriptions',
                }
            }).lean().then((user) => {
                if (arg.type == 'query') {
                    var users = [];
                    for (var i = 0; i < user.roster.subscriptions.length; i++) {
                        var sub = user.roster.subscriptions[i];
                        if (sub.status == 'accepted') {
                            users.push({
                                _id: sub.user._id,
                                username: sub.user.username,
                                displayName: sub.user.displayName,
                                profilePicture: sub.user.profilePicture,
                                online: !!connections[sub.user.username]
                            });
                        }
                    }
                    setTimeout(function() {
                        socket.emit("roster", {
                            type: "query",
                            result: users
                        });
                    }, 1000);
                } else if (arg.type == 'subscribe') {

                    User.findOne({
                        username: arg.to
                    }).populate({
                        path: 'roster',
                        populate: {
                            path: 'subscriptions',
                        }
                    }).lean().then((remoteUser) => {
                        var status = 'sent';
                        var foundInRemote = false;
                        for (var i = 0; i < remoteUser.roster.subscriptions.length; i++) {
                            var sub = remoteUser.roster.subscriptions[i];
                            if (sub.user._id.equals(user._id)) {
                                switch (sub.status) {
                                    case 'sent':
                                        remoteUser.roster.subscriptions[i].status = 'accepted';
                                        remoteUser.roster.save();
                                    case 'accepted':
                                        status = 'accepted';
                                        break;
                                    case 'received':
                                        status = 'sent';
                                        break;
                                    case 'blocked':
                                        status = 'blocked';
                                        break;
                                }
                                foundInRemote = true;

                                break;
                            }
                        }
                        if (!foundInRemote) {
                            remoteUser.roster.subscriptions.push({
                                status: 'received',
                                user: user
                            });
                            remoteUser.roster.save();


                        }

                        for (var i = 0; i < user.roster.subscriptions.length; i++) {
                            var sub = user.roster.subscriptions[i];
                            if (sub.user._id.equals(remoteUser._id)) {
                                user.roster.subscriptions[i].status = status;
                                user.roster.save();
                            }
                        }

                        if (status == 'sent' || status == 'accepted') {
                            var foundInUser = false;
                            for (var i = 0; i < user.roster.subscriptions.length; i++) {
                                var sub = user.roster.subscriptions[i];
                                if (sub.user._id.equals(remoteUser._id)) {
                                    user.roster.subscriptions[i].status = status;
                                    user.roster.save();
                                    foundInUser = true;
                                }
                            }
                            if (!foundInUser) {
                                user.roster.subscriptions.push({
                                    status: 'sent',
                                    user: remoteUser
                                });
                                user.roster.save();
                            }
                        } else if (status == 'blocked') {

                        }
                    });
                } else if (arg.type == 'unsubscribe') {

                } else if (arg.type == 'block') {

                } else {

                }
            }).catch((err) => {
                console.error(err);
            });
        })


        socket.on('message', (arg) => {
            console.log('message', arg);
            arg = Object.assign({
                id: uuidV4(),
                type: null,
                to: null,
                action: null
            }, arg);

            switch (arg.type) {
                case 'call':
                    processCall(arg, socket);
                    break;
                default:
                    break;
            }

        });

        socket.on('call', (arg) => {
            arg = Object.assign({
                type: 'call', // 'call', 'sdp', 'ice'
                to: null,
                action: null
            }, arg);

            processCall(arg, socket);

        })

        // socket.on('chat', (arg) => {
        //     arg = Object.assign({
        //         id: uuidV4(),
        //         from: getSocketUsername(socket),
        //         to: null,
        //         type: null,
        //         data: null,
        //         state: null,
        //         time: (new Date()).getTime()
        //     }, arg);
        //     processChat(arg, socket);
        // })

        socket.on('subscription', (arg) => {
            console.log('subscription', arg);
            User.findOne({
                    username: getSocketUsername(socket)
                })
                .populate({
                    path: 'roster',
                    populate: {
                        path: 'subscriptions'
                    }
                })
                .then((from) => {
                    if (from) {
                        if (arg.type == 'subscribe' || arg.type == 'unsubscribe') {
                            User.findOne({
                                    username: arg.to
                                })
                                .populate("roster")
                                .then((to) => {
                                    if (arg.type == 'subscribe') { // subscribe
                                        // find if already subscribed
                                        // search in roster
                                        var foundInRoster = false;
                                        for (var i = 0; i < from.roster.users.length; i++) {
                                            var rUser = from.roster.users[i];
                                            if (rUser.equals(to._id)) {
                                                foundInRoster = true;
                                                break;
                                            }
                                        }
                                        if (foundInRoster) { // already accepted
                                            console.log(from.username, "found", to.username, "in roster");
                                            socket.emit('subscription', {
                                                type: 'subscribe',
                                                from: from.username,
                                                to: to.username,
                                                status: 'accepted'
                                            })
                                            return;
                                        }
                                        // search in subscription
                                        var foundInSubscription = false;
                                        for (var i = 0; i < from.roster.subscriptions.length; i++) {
                                            var rSub = from.roster.subscriptions[i];
                                            if (rSub.to.equals(to._id)) { // already exists
                                                socket.emit('subscription', {
                                                    type: 'subscribe',
                                                    from: from.username,
                                                    to: to.username,
                                                    status: 'pending'
                                                })
                                                foundInSubscription = true;
                                                break;
                                            } else if (rSub.from.equals(to._id)) { // accept subscription
                                                rSub.status = 'accepted';
                                                rSub.save().then(() => {
                                                    from.roster.users.push(to._id);
                                                    to.roster.users.push(from._id);
                                                    from.roster.save();
                                                    to.roster.save();
                                                    var sub = {
                                                        type: 'subscribe',
                                                        from: from.username,
                                                        to: to.username,
                                                        status: 'accepted'
                                                    };

                                                    // join each other subs room
                                                    ioMain.to('user#' + arg.to).join('subs#' + arg.from);
                                                    ioMain.to('user#' + arg.from).join('subs#' + arg.to);

                                                    socket.to('user#' + arg.to).emit('subscription', sub);
                                                    socket.emit('subscription', sub);
                                                }).catch((err) => {
                                                    console.error(err);
                                                });
                                                foundInSubscription = true;
                                                break;
                                            }
                                        }
                                        if (!foundInSubscription) {
                                            // create new
                                            var newSub = new Subscription({
                                                from: from._id,
                                                to: to._id
                                            });
                                            newSub.save().then((sub) => {
                                                from.roster.subscriptions.push(sub._id);
                                                to.roster.subscriptions.push(sub._id);
                                                from.roster.save();
                                                to.roster.save();
                                                var sub = {
                                                    type: 'subscribe',
                                                    from: from.username,
                                                    to: to.username,
                                                    status: 'pending'
                                                };
                                                socket.to('user#' + arg.to).emit('subscription', sub);
                                                socket.emit('subscription', sub);
                                            });
                                        }


                                    } else { // unsubscribe
                                        // search in roster
                                        var foundInRoster = false;
                                        for (var i = 0; i < from.roster.users.length; i++) {
                                            var rUser = from.roster.users[i];
                                            if (rUser.equals(to._id)) {
                                                from.roster.users.splice(i, 1);
                                                foundInRoster = true;
                                                break;
                                            }
                                        }
                                        from.roster.save().then(() => {
                                            console.log('delete user', to.username, 'from', from.username, `'s roster`);
                                        });

                                        if (foundInRoster) {
                                            for (var i = 0; i < to.roster.users.length; i++) {
                                                var rUser = to.roster.users[i];
                                                if (rUser.equals(from._id)) {
                                                    to.roster.users.splice(i, 1);
                                                    break;
                                                }
                                            }
                                            to.roster.save().then(() => {
                                                console.log('delete user', from.username, 'from', to.username, `'s roster`);
                                            });
                                            var sub = {
                                                type: 'unsubscribe',
                                                from: from.username,
                                                to: to.username
                                            }
                                        }
                                        Subscription.remove({
                                            $or: [{
                                                    from: from._id,
                                                    to: to._id
                                                },
                                                {
                                                    to: from._id,
                                                    from: to._id
                                                },
                                            ]
                                        }).then(() => {
                                            var sub = {
                                                type: 'unsubscribe',
                                                from: from.username,
                                                to: to.username
                                            }
                                            socket.to('user#' + arg.to).emit('subscription', sub);
                                            socket.emit('subscription', sub);
                                        }).catch((err) => {
                                            console.error(err);
                                        });

                                        // leave each other subs room
                                        ioMain.to('user#' + arg.to).leave('subs#' + arg.from);
                                        ioMain.to('user#' + arg.from).leave('subs#' + arg.to);

                                        // socket.to('user#' + arg.to).emit('subscription', sub);
                                        // socket.emit('subscription', sub);

                                    }

                                }).catch((err) => {
                                    throw err;
                                });
                        } else if (arg.type == 'list') {
                            Subscription.find({
                                    $and: [{
                                        status: 'pending'
                                    }, {
                                        $or: [{
                                                from: from._id,
                                                status: 'pending'
                                            },
                                            {
                                                to: from._id,
                                                status: 'pending'
                                            },
                                        ]
                                    }],
                                })
                                .populate('from')
                                .populate('to')
                                .lean()
                                .then((subMs) => {
                                    var subs = [];
                                    for (var index = 0; index < subMs.length; index++) {
                                        var subM = subMs[index];
                                        subs.push({
                                            from: subM.from.username,
                                            to: subM.to.username,
                                            status: subM.status
                                        });
                                    }
                                    socket.emit('subscription', {
                                        type: 'list',
                                        data: subs
                                    });
                                }).catch((err) => {
                                    console.error(err);
                                });
                        }

                    }
                }).catch((err) => {
                    throw err;
                })
        });

        socket.on('profilePicture', (arg) => {
            User.findOne({
                username: getSocketUsername(socket)
            }).then((user) => {
                if (Buffer.isBuffer(arg)) {
                    var ft = fileType(arg);
                    if (ft.mime.startsWith('image')) {
                        var tempName = profilePictureDir + 'temp-' + Date.now();
                        console.log('profilePicture', 'tempName', tempName);
                        fs.writeFile(tempName, arg, (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            crypto.pseudoRandomBytes(16, function(err, raw) {
                                if (err) {
                                    console.error(err);
                                    return;
                                }
                                var fn = profilePictureDir + Date.now() + '-' + raw.toString('hex') + '.' + ft.ext;

                                console.log('profilePicture', 'newName', fn);
                                fs.rename(tempName, fn, (err) => {
                                    if (err) {
                                        console.error(err);
                                        return;
                                    }

                                    var newProfilePicturePath = fn.replace("public/", "/");
                                    user.profilePicture = newProfilePicturePath;
                                    user.save().then((doc) => {
                                        console.log("update user", doc);
                                        socket.emit('profilePicture', {
                                            status: true,
                                            data: newProfilePicturePath
                                        });
                                    }).catch((err) => {
                                        console.error(err);
                                    });
                                });

                            });
                        });
                    }
                } else if (typeof arg === 'string') {
                    console.log("profilePicture", "unsupported");
                } else {

                }
            }).catch((err) => {
                console.error(err);
            });
        })

        socket.on("displayName", (arg) => {
            User.update({
                username: getSocketUsername(socket)
            }, {
                displayName: arg
            }).then((doc) => {
                socket.emit('displayName', arg);
            }).catch((err) => {
                console.error(err);
            });
        });
    })

    function processCall(call, socket) {
        console.log('processCall', call);
        console.log('from', connections[call.from]);
        console.log('to', connections[call.to]);

        let username = getSocketUsername(socket);
        call.from = call.from || username;
        if (call.from == call.to) {

        } else if (connections[call.to] && connections[call.from]) {
            let target = call.to;
            if (username == call.from) {
                target = call.to;
            } else {
                target = call.from;
            }

            if (call.type == 'call') {
                if (call.action == 'calling') {
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                } else if (call.action == 'accept') {
                    if (username == call.from) return;
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                } else if (call.action == 'reject') {
                    socket.to('user#' + target).emit('call', call);
                    socket.emit('call', call);
                    return;
                }
            } else if (call.type == 'media') {
                if (call.action == 'ready') {
                    socket.to('user#' + target).emit('call', call);
                    return;
                } else if (call.action == 'sdp') {
                    console.log("send sdp to: ", target, " : ", call);
                    socket.to('user#' + target).emit('call', call);
                    return;
                } else if (call.action == 'ice') {
                    console.log("send ice to: ", target, " : ", call);
                    socket.to('user#' + target).emit('call', call);
                    return;
                }
            }
        } else {
            call.action = 'unavailable';
            socket.emit('call', call);
            return;
        }
        console.log('processCall', call, 'invalid');
        call.action = 'invalid';
        socket.emit('call', call);
    }

    function processChat(chat, socket) {
        var username = getSocketUsername(socket);
        var target = null;
        if (username == socket.from) { // regular
            target = chat.to;
            if (connections[target]) {
                socket.to("user#" + target).emit('chat', chat);
            } else {
                storeChat(chat, username, target);
            }
        } else { // status | ack
            target = username;
            if (connections[target]) {
                socket.to("user#" + target).emit('chat', chat);
            } else {
                storeChat(chat, username, target);
            }
        }
    }

    function storeChat(chat, username, target) {
        // TODO: implement store and send
    }

    function getSocketUsername(socket) {
        for (var room in socket.rooms) {
            if (socket.rooms.hasOwnProperty(room)) {
                if (room.startsWith('user#')) {
                    return room.substr(5);
                }
            }
        }
        return null;
    }
}



module.exports = socketMain;