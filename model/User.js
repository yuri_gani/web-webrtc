var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;
var Roster = require('./Roster');

var userSchema = new mongoose.Schema({
    username: { type: String, required: true, minlength: 3, maxlength: 30, trim: true, index: { unique: true } },
    password: { type: String, required: true, select: false },
    type: {type: String, enum: ['local', 'ldap', 'guest'], default: 'local'},
    displayName: String,
    profilePicture: String,
    status: { type: String, enum: ['active', 'inactive'], default: 'active' },
    roster: { type: Schema.Types.ObjectId, ref: 'Roster' },
    created: { type: Date, default: Date.now },
    updated: { type: Date },
    deleted: { type: Date }
});

userSchema.pre('save', function(next) {
    var user = this;
    console.log(this);
    if (!this.roster) {
        this.roster = new Roster({
            owner: this,
            users: []
        });
        this.roster.save();
    }

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

// userSchema.post('save', function() {
//     if (this.roster) {
//         if (this.roster.owner) {

//         } else {
//             this.roster.owner = this;
//             this.roster.save();
//         }
//     } else {
//         this.roster = new Roster({
//             owner: this
//         });
//         this.save();
//     }
// });

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', userSchema);