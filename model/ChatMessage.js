var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

var chatMessageSchema = new mongoose.Schema({
    from: { type: Schema.Types.ObjectId, ref: 'User' },
    room: { type: Schema.Types.ObjectId, ref: 'ChatRoom' },
    created: { type: Date, default: Date.now },
    updated: { type: Date },
    deleted: { type: Date }
});

module.exports = mongoose.model('ChatMessage', chatMessageSchema);