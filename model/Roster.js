var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

var rosterSchema = new mongoose.Schema({
    owner: { type: Schema.Types.ObjectId, ref: 'User', index: { unique: true } },
    users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    subscriptions: [{ type: Schema.Types.ObjectId, ref: 'Subscription' }],
    created: { type: Date, default: Date.now },
    updated: { type: Date },
    deleted: { type: Date }
});

module.exports = mongoose.model('Roster', rosterSchema);