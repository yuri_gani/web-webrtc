var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

var subscriptonSchema = new mongoose.Schema({
    from: { type: Schema.Types.ObjectId, ref: 'User' },
    to: { type: Schema.Types.ObjectId, ref: 'User' },
    status: { type: String, enum: ['pending', 'blocked', 'accepted'], default: 'pending' },
    message: { type: String },
    created: { type: Date, default: Date.now },
    updated: { type: Date },
    deleted: { type: Date }
});

module.exports = mongoose.model('Subscription', subscriptonSchema);