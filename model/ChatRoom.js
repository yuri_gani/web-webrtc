var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

var chatRoomSchema = new mongoose.Schema({
    participants: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    messages: [{ type: Schema.Types.ObjectId, ref: 'ChatMessage' }],
    created: { type: Date, default: Date.now },
    updated: { type: Date },
    deleted: { type: Date }
});

module.exports = mongoose.model('ChatRoom', chatRoomSchema);