var config = require('./config.js');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


// session
var { Passport, Strategy } = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var session = require('express-session');
var RedisStore = require('connect-redis')(session);

var index = require('./routes/index');
var users = require('./routes/users');
var login = require('./routes/login');
var admin = require('./routes/admin');
var mobile = require('./routes/mobile');
var callcs = require('./routes/callcs');

var app = express();

// session
global.sessionStore = sessionStore = new RedisStore({
    url: config.redis.url
});
app.use(session({
    store: sessionStore,
    secret: config.redis.secret,
    resave: true,
    saveUninitialized: true
}));
// var passport = new Passport();


// app.use(passport.initialize());
// app.use(passport.session());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.engine('html', require('ejs').renderFile);
app.set('html', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



app.use('/admin', admin);
app.use('/mobile', mobile);
app.use('/callcs', callcs);

app.use('/', index.auth, index);
app.use('/users', users);
app.use('/login', login);

app.use('/logout', function(req, res) {
    console.log('logging out', req.session.username);
    req.session.destroy();
    res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error(req.path + ' Not Found');
    err.status = 404;
    next(err);
});



// error handler
// app.use(function(err, req, res, next) {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//     res.locals.error = req.app.get('env') === 'development' ? err : {};

//     // render the error page
//     res.status(err.status || 500);
//     res.render('error');
// });

app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    console.log(err);

    // render the error page
    res.status(err.status || 500);
    res.send('error');
});

module.exports = app;